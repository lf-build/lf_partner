﻿using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Identity;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Partner.Test
{
    public class PartnerServiceTest
    {

        private static FakePartnerRepository GetFakePartnerRepository(List<IPartner> partnerDetails = null)
        {
            return new FakePartnerRepository(new UtcTenantTime(), partnerDetails ?? new List<IPartner>());
        }

        private static FakePartnerUserRepository GetFakePartnerUserRepository(List<IPartnerUser> partnerUserDetails = null)
        {
            return new FakePartnerUserRepository(new UtcTenantTime(), partnerUserDetails ?? new List<IPartnerUser>());
        }

        private static IPartnerService GetPartnerService(IPartnerRepository partnerRepository, IPartnerUserRepository partnerUserRepo, IPartnerConfigurations partnerConfigurations = null)
        {
            return new PartnerService(partnerRepository, Mock.Of<IEventHubClient>(), partnerUserRepo, Mock.Of<LendFoundry.Security.Identity.Client.IIdentityService>(), partnerConfigurations, Mock.Of<ITenantTime>(), null,null,null,null);
        }
        [Fact]
        public async Task Partner_Add_ThowArgumentException()
        {
            IPartner partner = new Partner();

            var service = GetPartnerService(GetFakePartnerRepository(), GetFakePartnerUserRepository());

            await Assert.ThrowsAsync<InvalidArgumentException>(
                () => service.Add(null));

            await Assert.ThrowsAsync<InvalidArgumentException>(
                () => service.Add(partner));
            partner.Name = "name1";
            await Assert.ThrowsAsync<InvalidArgumentException>(
               () => service.Add(partner));
            Address address = new Address();
            partner.Address = address;
            await Assert.ThrowsAsync<InvalidArgumentException>(
               () => service.Add(partner));

        }

        [Fact]
        public async Task Partner_Add_Success()
        {
            IPartner partner = new Partner();
            List<IContact> contact = new List<IContact>();
            IContact contact1 = new Contact();
            contact.Add(contact1);
            partner.Name = "name1";
            Address address = new Address();
            address.Line1 = "test";
            partner.Address = address;
            partner.Contacts = contact;
            var service = GetPartnerService(GetFakePartnerRepository(), GetFakePartnerUserRepository());

            var result = await service.Add(partner);
            Assert.NotNull(result);

        }

        [Fact]
        public async Task Partner_Remove_ThowArgumentException()
        {
            IPartner partner = new Partner();
            var fakePartnerRepository = GetFakePartnerRepository(new List<IPartner> { new Partner
            {
               Id = "1",
               Name = "test"
            } });
            var service = GetPartnerService(fakePartnerRepository, GetFakePartnerUserRepository());

            await Assert.ThrowsAsync<InvalidArgumentException>(
                () => service.Remove(null));

            await Assert.ThrowsAsync<NotFoundException>(
                () => service.Remove("2"));
        }


        [Fact]
        public async Task Partner_Remove_Success()
        {
            IPartner partner = new Partner();
            var fakePartnerRepository = GetFakePartnerRepository(new List<IPartner> { new Partner
            {
               Id = "588720c06da0432374a4c39e",
               Name = "test"
            } });
            var service = GetPartnerService(fakePartnerRepository, GetFakePartnerUserRepository());

            await service.Remove("588720c06da0432374a4c39e");
        }

        [Fact]
        public async Task Partner_Update_ThowArgumentException()
        {

            var fakePartnerRepository = GetFakePartnerRepository(new List<IPartner> { new Partner
            {
               Id = "588720c06da0432374a4c39e",
               Name = "test"
            } });
            var service = GetPartnerService(fakePartnerRepository, GetFakePartnerUserRepository());

            await Assert.ThrowsAsync<InvalidArgumentException>(
                () => service.Update(null, null));

            await Assert.ThrowsAsync<NotFoundException>(
                () => service.Update("2", null));

            await Assert.ThrowsAsync<InvalidArgumentException>(
             () => service.Update("588720c06da0432374a4c39e", null));

            IPartner partner = new Partner();

            await Assert.ThrowsAsync<InvalidArgumentException>(
           () => service.Update("588720c06da0432374a4c39e", partner));

            partner.Name = "name1";
            await Assert.ThrowsAsync<InvalidArgumentException>(
               () => service.Update("588720c06da0432374a4c39e", partner));
            Address address = new Address();
            partner.Address = address;
            await Assert.ThrowsAsync<InvalidArgumentException>(
               () => service.Update("588720c06da0432374a4c39e", partner));
        }

        [Fact]
        public async Task Partner_Update_Success()
        {

            var fakePartnerRepository = GetFakePartnerRepository(new List<IPartner> { new Partner
            {
               Id = "588720c06da0432374a4c39e",
               Name = "test"
            } });

            var service = GetPartnerService(fakePartnerRepository, GetFakePartnerUserRepository());
            IPartner partner = new Partner();
            List<IContact> contact = new List<IContact>();
            IContact contact1 = new Contact();
            contact.Add(contact1);
            partner.Name = "name1";
            Address address = new Address();
            address.Line1 = "test";
            partner.Address = address;
            partner.Contacts = contact;
            await service.Update("588720c06da0432374a4c39e", partner);

        }

        [Fact]
        public async Task Partner_GetAll_Success()
        {

            var fakePartnerRepository = GetFakePartnerRepository(new List<IPartner> { new Partner
            {
               Id = "588720c06da0432374a4c39e",
               Name = "test"
            } });

            var service = GetPartnerService(fakePartnerRepository, GetFakePartnerUserRepository());

            var result = await service.GetAll();
            Assert.NotNull(result);

        }

        [Fact]
        public async Task Partner_AddContacts_ThrowException()
        {

            var fakePartnerRepository = GetFakePartnerRepository(new List<IPartner> { new Partner
            {
               Id = "588720c06da0432374a4c39e",
               Name = "test"
            } });

            var service = GetPartnerService(fakePartnerRepository, GetFakePartnerUserRepository());

            IContact contact = new Contact();

            await Assert.ThrowsAsync<ArgumentException>(
                () => service.AddContacts(null, null));

            await Assert.ThrowsAsync<ArgumentException>(
                () => service.AddContacts("2", null));

            await Assert.ThrowsAsync<ArgumentNullException>(
                () => service.AddContacts("2", contact));

            contact.FirstName = "test";

            await Assert.ThrowsAsync<ArgumentNullException>(
               () => service.AddContacts("2", contact));

            contact.Email = "testEmail";

            await Assert.ThrowsAsync<NotFoundException>(
               () => service.AddContacts("588720c06da0432374a4c39m", contact));



        }


        [Fact]
        public async Task Partner_AddContacts_Success()
        {

            var fakePartnerRepository = GetFakePartnerRepository(new List<IPartner> { new Partner
            {
               Id = "588720c06da0432374a4c39e",
               Name = "test"
            } });

            var service = GetPartnerService(fakePartnerRepository, GetFakePartnerUserRepository());

            IContact contact = new Contact();
            contact.FirstName = "test";
            contact.Email = "testEmail";

            var result = await service.AddContacts("588720c06da0432374a4c39e", contact);
            Assert.NotNull(result);

        }

        [Fact]
        public async Task Partner_UpdateContacts_ThrowException()
        {

            var fakePartnerRepository = GetFakePartnerRepository(new List<IPartner> { new Partner
            {
               Id = "588720c06da0432374a4c39e",
               Name = "test"
            } });

            var service = GetPartnerService(fakePartnerRepository, GetFakePartnerUserRepository());

            IContact contact = new Contact();

            await Assert.ThrowsAsync<ArgumentException>(
                () => service.UpdateContacts(null, null, null));

            await Assert.ThrowsAsync<ArgumentException>(
                () => service.UpdateContacts("2", null, null));

            await Assert.ThrowsAsync<ArgumentException>(
                () => service.UpdateContacts("2", "1", null));

            contact.FirstName = "test";

            await Assert.ThrowsAsync<ArgumentNullException>(
               () => service.UpdateContacts("2", "1", contact));

            contact.Email = "testEmail";

            await Assert.ThrowsAsync<NotFoundException>(
               () => service.UpdateContacts("1", "1", contact));

        }


        [Fact]
        public async Task Partner_UpdateContacts_Success()
        {

            var fakePartnerRepository = GetFakePartnerRepository(new List<IPartner> { new Partner
            {
               Id = "588720c06da0432374a4c39e",
               Name = "test"
            } });

            var service = GetPartnerService(fakePartnerRepository, GetFakePartnerUserRepository());

            IContact contact = new Contact();
            contact.FirstName = "test";
            contact.Email = "testEmail";

            await service.UpdateContacts("588720c06da0432374a4c39e", "1", contact);

        }

        [Fact]
        public async Task Partner_DeleteContact_ThrowException()
        {

            var fakePartnerRepository = GetFakePartnerRepository(new List<IPartner> { new Partner
            {
               Id = "588720c06da0432374a4c39e",
               Name = "test"
            } });

            var service = GetPartnerService(fakePartnerRepository, GetFakePartnerUserRepository());

            IContact contact = new Contact();

            await Assert.ThrowsAsync<ArgumentException>(
                () => service.DeleteContact(null, null));

            await Assert.ThrowsAsync<ArgumentException>(
                () => service.DeleteContact("2", null));

            await Assert.ThrowsAsync<NotFoundException>(
                () => service.DeleteContact("588720c06da0432374a4c39", "1"));

            contact.FirstName = "test";

            await Assert.ThrowsAsync<NotFoundException>(
               () => service.DeleteContact("588720c06da0432374a4c39e", "1"));

        }

        [Fact]
        public async Task Partner_DeleteContact_Success()
        {
            List<IPartner> partner = new List<IPartner>();
            Partner partnerObj = new Partner();
            partnerObj.Id = "588720c06da0432374a4c39e";
            partnerObj.Name = "test";
            List<IContact> contact = new List<IContact>();
            Contact contactObj = new Contact();
            List<IContact> listOfContact = new List<IContact>();
            contactObj.ContactId = "1";
            listOfContact.Add(contactObj);
            partnerObj.Contacts = listOfContact;

            partner.Add(partnerObj);
            var fakePartnerRepository = GetFakePartnerRepository(partner);

            var service = GetPartnerService(fakePartnerRepository, GetFakePartnerUserRepository());

            await service.DeleteContact("588720c06da0432374a4c39e", "1");

        }

        [Fact]
        public async Task Partner_Update_ThrowException()
        {

            var fakePartnerRepository = GetFakePartnerRepository(new List<IPartner> { new Partner
            {
               Id = "588720c06da0432374a4c39e",
               Name = "test"
            } });

            var service = GetPartnerService(fakePartnerRepository, GetFakePartnerUserRepository());

            IPartner partner = new Partner();

            await Assert.ThrowsAsync<InvalidArgumentException>(
                () => service.Update(null, null));

            await Assert.ThrowsAsync<NotFoundException>(
                () => service.Update("2", null));

            await Assert.ThrowsAsync<NotFoundException>(
                () => service.Update("588720c06da0432374a4c39", null));

            await Assert.ThrowsAsync<InvalidArgumentException>(
               () => service.Update("588720c06da0432374a4c39e", partner));

            partner.Name = "test";

            await Assert.ThrowsAsync<InvalidArgumentException>(
               () => service.Update("588720c06da0432374a4c39e", partner));

            IAddress address = new Address();
            partner.Address = address;

            await Assert.ThrowsAsync<InvalidArgumentException>(
            () => service.Update("588720c06da0432374a4c39e", partner));

        }


        [Fact]
        public async Task Partner_UpdateActiveContact_ThrowException()
        {
            List<IPartner> partner = new List<IPartner>();
            Partner partnerObj = new Partner();
            partnerObj.Id = "588720c06da0432374a4c39e";
            partnerObj.Name = "test";
            List<IContact> contact = new List<IContact>();
            Contact contactObj = new Contact();
            List<IContact> listOfContact = new List<IContact>();
            contactObj.ContactId = "1";
            listOfContact.Add(contactObj);
            partnerObj.Contacts = listOfContact;
            partner.Add(partnerObj);

            var fakePartnerRepository = GetFakePartnerRepository(partner);

            var service = GetPartnerService(fakePartnerRepository, GetFakePartnerUserRepository());


            await Assert.ThrowsAsync<ArgumentNullException>(
                () => service.UpdateActiveContact(null, null));

            await Assert.ThrowsAsync<ArgumentNullException>(
                () => service.UpdateActiveContact("2", null));

            IContactUpdateRequest contactupdaterequest = new ContactUpdateRequest();

            await Assert.ThrowsAsync<ArgumentException>(
                () => service.UpdateActiveContact("588720c06da0432374a4c39", contactupdaterequest));

            contactupdaterequest.FirstName = "test";
            await Assert.ThrowsAsync<ArgumentException>(
              () => service.UpdateActiveContact("588720c06da0432374a4c39", contactupdaterequest));

            contactupdaterequest.LastName = "test";
            await Assert.ThrowsAsync<ArgumentException>(
            () => service.UpdateActiveContact("588720c06da0432374a4c39", contactupdaterequest));

            contactupdaterequest.JobTitle = "test";

            await Assert.ThrowsAsync<ArgumentException>(
               () => service.UpdateActiveContact("588720c06da0432374a4c39e", contactupdaterequest));

            contactupdaterequest.Email = "test";

            await Assert.ThrowsAsync<ArgumentException>(
             () => service.UpdateActiveContact("588720c06da0432374a4c39e", contactupdaterequest));

            contactupdaterequest.PhoneNumber = "test";

            await Assert.ThrowsAsync<NotFoundException>(
               () => service.UpdateActiveContact("1", contactupdaterequest));

            await Assert.ThrowsAsync<NotFoundException>(
            () => service.UpdateActiveContact("588720c06da0432374a4c39e", contactupdaterequest));

        }

        [Fact]
        public async Task Partner_UpdateActiveContact_Success()
        {
            List<IPartner> partner = new List<IPartner>();
            Partner partnerObj = new Partner();
            partnerObj.Id = "588720c06da0432374a4c39e";
            partnerObj.Name = "test";
            List<IContact> contact = new List<IContact>();
            Contact contactObj = new Contact();
            List<IContact> listOfContact = new List<IContact>();
            contactObj.ContactId = "1";
            contactObj.IsPrimary = true;
            listOfContact.Add(contactObj);
            partnerObj.Contacts = listOfContact;
            partner.Add(partnerObj);

            var fakePartnerRepository = GetFakePartnerRepository(partner);

            var service = GetPartnerService(fakePartnerRepository, GetFakePartnerUserRepository());

            IContactUpdateRequest contactupdaterequest = new ContactUpdateRequest();

            contactupdaterequest.FirstName = "test";
            contactupdaterequest.LastName = "test";

            contactupdaterequest.JobTitle = "test";

            contactupdaterequest.Email = "test";

            contactupdaterequest.PhoneNumber = "test";
            await service.UpdateActiveContact("588720c06da0432374a4c39e", contactupdaterequest);

        }

        [Fact]
        public async Task Partner_DeletePartnerUser_ThrowException()
        {

            var fakePartnerRepository = GetFakePartnerRepository(new List<IPartner> { new Partner
            {
               Id = "588720c06da0432374a4c39e",
               Name = "test"
            } });

            var service = GetPartnerService(fakePartnerRepository, GetFakePartnerUserRepository());

            IPartner partner = new Partner();

            await Assert.ThrowsAsync<InvalidArgumentException>(
                () => service.DeletePartnerUser(null, null));

            await Assert.ThrowsAsync<InvalidArgumentException>(
                () => service.DeletePartnerUser("2", null));

            await Assert.ThrowsAsync<NotFoundException>(
                () => service.DeletePartnerUser("588720c06da0432374a4c39", "1"));

        }

        [Fact]
        public async Task Partner_DeletePartnerUser_Success()
        {

            var fakePartnerRepository = GetFakePartnerRepository(new List<IPartner> { new Partner
            {
               Id = "588720c06da0432374a4c39e",
               Name = "test"
            } });

            var fakePartnerUserRepository = GetFakePartnerUserRepository(new List<IPartnerUser> { new PartnerUser
            {
               Id = "588720c06da0432374a4c39e"
               
            } });

            var service = GetPartnerService(fakePartnerRepository, fakePartnerUserRepository);
            
            await  service.DeletePartnerUser("588720c06da0432374a4c39e", "588720c06da0432374a4c39e");

        }

        [Fact]
        public async Task Partner_AddPartnerUser_ThrowException()
        {

            var fakePartnerRepository = GetFakePartnerRepository(new List<IPartner> { new Partner
            {
               Id = "588720c06da0432374a4c39e",
               Name = "test"
            } });

            var service = GetPartnerService(fakePartnerRepository, GetFakePartnerUserRepository());

            IPartner partner = new Partner();

            await Assert.ThrowsAsync<InvalidArgumentException>(
                () => service.AddPartnerUser(null, null));

            await Assert.ThrowsAsync<NotFoundException>(
                () => service.AddPartnerUser("2", null));

            IPartnerUserRequest partnerUserrequest = new PartnerUserRequest();

            await Assert.ThrowsAsync<InvalidArgumentException>(
                () => service.AddPartnerUser("588720c06da0432374a4c39e", null));

            await Assert.ThrowsAsync<InvalidOperationException>(
              () => service.AddPartnerUser("588720c06da0432374a4c39e", partnerUserrequest));
            partnerUserrequest.UserName = "test";

            await Assert.ThrowsAsync<InvalidOperationException>(
              () => service.AddPartnerUser("588720c06da0432374a4c39e", partnerUserrequest));
            partnerUserrequest.Password = "test";

            await Assert.ThrowsAsync<InvalidOperationException>(
             () => service.AddPartnerUser("588720c06da0432374a4c39e", partnerUserrequest));

            partnerUserrequest.Email = "test";

          
        }

        [Fact]
        public async Task Partner_AssosiateUserToPartner_ThrowException()
        {

            var fakePartnerRepository = GetFakePartnerRepository(new List<IPartner> { new Partner
            {
               Id = "588720c06da0432374a4c39e",
               Name = "test"
            } });

            var service = GetPartnerService(fakePartnerRepository, GetFakePartnerUserRepository());

            IPartner partner = new Partner();

            await Assert.ThrowsAsync<InvalidArgumentException>(
                () => service.AssosiateUserToPartner(null, null));

            await Assert.ThrowsAsync<InvalidArgumentException>(
                () => service.AssosiateUserToPartner("2", null));

            await Assert.ThrowsAsync<NotFoundException>(
                () => service.AssosiateUserToPartner("2", "1"));
            
        }

        [Fact]
        public async Task Partner_AttachUser_ThrowException()
        {
            List<IPartner> partner = new List<IPartner>();
            Partner partnerObj = new Partner();
            partnerObj.Id = "588720c06da0432374a4c39e";
            partnerObj.Name = "test";
            List<IContact> contact = new List<IContact>();
            Contact contactObj = new Contact();
            List<IContact> listOfContact = new List<IContact>();
            contactObj.ContactId = "1";
            contactObj.IsPrimary = true;
            listOfContact.Add(contactObj);
            partnerObj.Contacts = listOfContact;
            partner.Add(partnerObj);

            var fakePartnerRepository = GetFakePartnerRepository(partner);

            var service = GetPartnerService(fakePartnerRepository, GetFakePartnerUserRepository());

            IPartnerUserRequest partnerRequest = new PartnerUserRequest();

            await Assert.ThrowsAsync<ArgumentException>(
                () => service.AttachUser(null, null,null));

            await Assert.ThrowsAsync<ArgumentException>(
                () => service.AttachUser("2", null,null));

            await Assert.ThrowsAsync<ArgumentException>(
                () => service.AttachUser("2", "1", partnerRequest));
            partnerRequest.UserName = "test";

            await Assert.ThrowsAsync<ArgumentException>(
            () => service.AttachUser("2", "1", partnerRequest));
            partnerRequest.Password = "test";

            await Assert.ThrowsAsync<NotFoundException>(
          () => service.AttachUser("2", "1", partnerRequest));

            await Assert.ThrowsAsync<NotFoundException>(
            () => service.AttachUser("588720c06da0432374a4c39e", "2", partnerRequest));

        }

        [Fact]
        public async Task Partner_DetachUser_ThrowException()
        {
            List<IPartner> partner = new List<IPartner>();
            Partner partnerObj = new Partner();
            partnerObj.Id = "588720c06da0432374a4c39e";
            partnerObj.Name = "test";
            List<IContact> contact = new List<IContact>();
            Contact contactObj = new Contact();
            List<IContact> listOfContact = new List<IContact>();
            contactObj.ContactId = "1";
            contactObj.IsPrimary = true;
            listOfContact.Add(contactObj);
            partnerObj.Contacts = listOfContact;
            partner.Add(partnerObj);

            var fakePartnerRepository = GetFakePartnerRepository(partner);

            var service = GetPartnerService(fakePartnerRepository, GetFakePartnerUserRepository());

            IPartnerUserRequest partnerRequest = new PartnerUserRequest();

            await Assert.ThrowsAsync<ArgumentException>(
                () => service.DetachUser(null, null, null));

            await Assert.ThrowsAsync<ArgumentException>(
                () => service.DetachUser("2", null, null));

            await Assert.ThrowsAsync<ArgumentException>(
                () => service.DetachUser("2", "1", null));
           

            await Assert.ThrowsAsync<NotFoundException>(
          () => service.DetachUser("2", "1", "1"));

            await Assert.ThrowsAsync<NotFoundException>(
            () => service.DetachUser("588720c06da0432374a4c39e", "2", "2"));
        }

        [Fact]
        public async Task Partner_DetachUser_Sucess()
        {
            List<IPartner> partner = new List<IPartner>();
            Partner partnerObj = new Partner();
            partnerObj.Id = "588720c06da0432374a4c39e";
            partnerObj.Name = "test";
            List<IContact> contact = new List<IContact>();
            Contact contactObj = new Contact();
            List<IContact> listOfContact = new List<IContact>();
            contactObj.ContactId = "1";
            contactObj.IsPrimary = true;
            listOfContact.Add(contactObj);
            partnerObj.Contacts = listOfContact;
            partner.Add(partnerObj);

            var fakePartnerUserRepository = GetFakePartnerUserRepository(new List<IPartnerUser> { new PartnerUser
            {
               Id = "588720c06da0432374a4c39e"

            } });

            var fakePartnerRepository = GetFakePartnerRepository(partner);

            var service = GetPartnerService(fakePartnerRepository, fakePartnerUserRepository);
           
            await service.DetachUser("588720c06da0432374a4c39e", "1", "588720c06da0432374a4c39e");
        }

     
    }
}
