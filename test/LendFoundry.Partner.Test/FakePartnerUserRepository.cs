﻿using LendFoundry.Foundation.Date;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace LendFoundry.Partner.Test
{

    public class FakePartnerUserRepository : IPartnerUserRepository
    {

        #region Constructors
        public FakePartnerUserRepository(ITenantTime tenantTime, List<IPartnerUser> partnerObj) : this(tenantTime)
        {
            PartnerDetail.AddRange(partnerObj);
        }
        public FakePartnerUserRepository(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
        }

        public ITenantTime TenantTime { get; set; }
        public List<IPartnerUser> PartnerDetail { get; } = new List<IPartnerUser>();

        public  Task<IPartnerUser> GetByUserId(string userId)
        {
            var result = Task.FromResult(PartnerDetail.Where(i => i.UserId.Equals(userId)).FirstOrDefault());
            return result;
        }

        public async Task<IPartnerUser> GetByPartnerandUserId(string partnerId, string userId)
        {
            if (partnerId == "588720c06da0432374a4c39e" && userId == "588720c06da0432374a4c39e")
                return PartnerDetail.FirstOrDefault();
            else
                return null;
          
        }

        public  Task<bool> Any(string partnerId, string userId)
        {
            var result = Task.FromResult(PartnerDetail.Where(i => i.PartnerId.Equals(partnerId) && i.UserId.Equals(userId)).Any());
            return result;
        }

        public Task<List<IPartnerUser>> GetByPartnerId(string partnerId)
        {
            var result = Task.FromResult(PartnerDetail.Where(i => i.PartnerId.Equals(partnerId)).ToList());
            return result;
        }

        public Task<IPartnerUser> Get(string id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<IPartnerUser>> All(Expression<Func<IPartnerUser, bool>> query, int? skip = default(int?), int? quantity = default(int?))
        {
            throw new NotImplementedException();
        }

        public void Add(IPartnerUser item)
        {
            throw new NotImplementedException();
        }

        public void Remove(IPartnerUser item)
        {
          
        }

        public void Update(IPartnerUser item)
        {
            throw new NotImplementedException();
        }

        public int Count(Expression<Func<IPartnerUser, bool>> query)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
