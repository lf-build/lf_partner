﻿using LendFoundry.Foundation.Date;
using System;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;
using MongoDB.Bson;

namespace LendFoundry.Partner.Test
{
    public class FakePartnerRepository : IPartnerRepository
    {
        #region Constructors
        public FakePartnerRepository(ITenantTime tenantTime, List<IPartner> partnerObj) : this(tenantTime)
        {
            PartnerDetail.AddRange(partnerObj);
        }
        public FakePartnerRepository(ITenantTime tenantTime)
        {
            TenantTime = tenantTime;
        }
        #endregion

        public ITenantTime TenantTime { get; set; }
        public List<IPartner> PartnerDetail { get; } = new List<IPartner>();

        public async  Task<IEnumerable<IPartner>> GetAll()
        {
            return  PartnerDetail;
        }

        public async Task<bool> Any(string partnerId)
        {
            if (partnerId == "588720c06da0432374a4c39e")
                return true;
            else
                return false;
        }

        public async Task<IPartner> GetByPartnerId(string id)
        {
            if (id == "588720c06da0432374a4c39e")
                return PartnerDetail.FirstOrDefault();
            else
                return null;

        }

        public void Update(string partnerId, IPartner partner)
        {
            
        }

        public Task UpdateContacts(string partnerId, List<IContact> contacts)
        {
            var result = Task.FromResult(PartnerDetail.FirstOrDefault());
            return result;
        }

        public Task<IPartner> Get(string id)
        {
            var result = Task.FromResult(PartnerDetail.Where(i => i.Id.Equals(id)).FirstOrDefault());
            return result;
        }

        public Task<IEnumerable<IPartner>> All(Expression<Func<IPartner, bool>> query, int? skip = default(int?), int? quantity = default(int?))
        {
            throw new NotImplementedException();
        }

        public void Add(IPartner item)
        {
            
        }

        public void Remove(IPartner item)
        {

        }

        public void Update(IPartner item)
        {
         
        }

        public int Count(Expression<Func<IPartner, bool>> query)
        {
            throw new NotImplementedException();
        }
    }
}
