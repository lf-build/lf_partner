﻿using LendFoundry.Foundation.Services;
using Moq;
using RestSharp;

using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;

namespace LendFoundry.Partner.Client.Tests
{
    public class PartnerClientTest
    {
        public IPartnerService PartnerService { get; set; }

        public IRestRequest Request { get; set; }

        public Mock<IServiceClient> MockServiceClient { get; set; }
        public PartnerClientTest()
        {
            MockServiceClient = new Mock<IServiceClient>();
            PartnerService = new PartnerService(MockServiceClient.Object);
        }

        [Fact]
        public async Task Client_Add()
        {
            Partner partnerObj = new Partner();
            MockServiceClient.Setup(s => s.ExecuteAsync<Partner>(It.IsAny<IRestRequest>()))
            .ReturnsAsync(partnerObj)
             .Callback<IRestRequest>(r => Request = r);
            
            await PartnerService.Add(partnerObj);
            Assert.Equal("/", Request.Resource);
            Assert.Equal(RestSharp.Method.POST, Request.Method);
        }


        [Fact]
        public async void Client_UpdateContacts()
        {

            MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r).Returns(
                      () =>
                        Task.FromResult(
                           true));
            IContact contactObj = new Contact();
            await PartnerService.UpdateContacts("productId", "contractId", contactObj);
            Assert.Equal("/{partnerId}/contact/{contactId}", Request.Resource);
            Assert.Equal(RestSharp.Method.PUT, Request.Method);
        }

        [Fact]
        public async void Client_DeleteContact()
        {

            MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r).Returns(
                      () =>
                        Task.FromResult(
                           true));
            IContact contactObj = new Contact();
            await PartnerService.DeleteContact("productId", "contractId");
            Assert.Equal("/{partnerId}/contact/{contactId}", Request.Resource);
            Assert.Equal(RestSharp.Method.DELETE, Request.Method);
        }

        [Fact]
        public async void Client_AttachUser()
        {

            MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r).Returns(
                      () =>
                        Task.FromResult(
                           true));
            IPartnerUserRequest partnerUserObj = new PartnerUserRequest();
            await PartnerService.AttachUser("productId", "contractId", partnerUserObj);
            Assert.Equal("/{partnerId}/attach/{contactId}", Request.Resource);
            Assert.Equal(RestSharp.Method.PUT, Request.Method);
        }

        [Fact]
        public async void Client_DetachUser()
        {

            MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r).Returns(
                      () =>
                        Task.FromResult(
                           true));
            IPartnerUserRequest partnerUserObj = new PartnerUserRequest();
            await PartnerService.DetachUser("productId", "contractId", "userId");
            Assert.Equal("/{partnerId}/Detach/{contactId}/{userId}", Request.Resource);
            Assert.Equal(RestSharp.Method.PUT, Request.Method);
        }


        [Fact]
        public async void Client_Update()
        {

            MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r).Returns(
                      () =>
                        Task.FromResult(
                           true));
            IPartner partnerObj = new Partner();
            await PartnerService.Update("productId", partnerObj);
            Assert.Equal("/{id}", Request.Resource);
            Assert.Equal(RestSharp.Method.PUT, Request.Method);

        }


        [Fact]
        public async void Client_AssosiateUserToPartner()
        {

            MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r).Returns(
                      () =>
                        Task.FromResult(
                           true));
            await PartnerService.AssosiateUserToPartner("productId", "userId");
            Assert.Equal("/{partnerId}/users/{userId}", Request.Resource);
            Assert.Equal(RestSharp.Method.POST, Request.Method);

        }

        [Fact]
        public async void Client_DeletePartnerUser()
        {

            MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r).Returns(
                      () =>
                        Task.FromResult(
                           true));
            IPartner partnerObj = new Partner();
            await PartnerService.DeletePartnerUser("productId", "userId");
            Assert.Equal("/{partnerId}/users/{userId}", Request.Resource);
            Assert.Equal(RestSharp.Method.DELETE, Request.Method);

        }


        [Fact]
        public async void Client_UpdateActiveContact()
        {

            MockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => Request = r).Returns(
                      () =>
                        Task.FromResult(
                           true));
            IContactUpdateRequest partnerObj = new ContactUpdateRequest();
            await PartnerService.UpdateActiveContact("productId", partnerObj);
            Assert.Equal("/{partnerId}/active-contact", Request.Resource);
            Assert.Equal(RestSharp.Method.PUT, Request.Method);

        }


        [Fact]
        public async void Client_AddContacts()
        {
            Partner partnerObj = new Partner();
            MockServiceClient.Setup(s => s.ExecuteAsync<Partner>(It.IsAny<IRestRequest>()))
               .ReturnsAsync(partnerObj)
                .Callback<IRestRequest>(r => Request = r);



            IContact contactObj = new Contact();
            var result = await PartnerService.AddContacts("productId", contactObj);
            Assert.Equal("/{partnerId}/contact", Request.Resource);
            Assert.Equal(RestSharp.Method.POST, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public async void Client_GetAll()
        {
            List<IPartner> partnerObj = new List<IPartner>();
            MockServiceClient.Setup(s => s.ExecuteAsync<List<IPartner>>(It.IsAny<IRestRequest>()))
               .ReturnsAsync(partnerObj)
                .Callback<IRestRequest>(r => Request = r);

            var result = await PartnerService.GetAll();
            Assert.Equal("/all", Request.Resource);
            Assert.Equal(RestSharp.Method.GET, Request.Method);
            Assert.NotNull(result);
        }

        [Fact]
        public async void Client_Get()
        {
            Partner partnerObj = new Partner();
            MockServiceClient.Setup(s => s.ExecuteAsync<Partner>(It.IsAny<IRestRequest>()))
               .ReturnsAsync(partnerObj)
                .Callback<IRestRequest>(r => Request = r);

            var result = await PartnerService.Get("partnerId");
            Assert.Equal("/{id}", Request.Resource);
            Assert.Equal(RestSharp.Method.GET, Request.Method);
            Assert.NotNull(result);
        }


        [Fact]
        public async void Client_AddPartnerUser()
        {
            PartnerUser partnerObj = new PartnerUser();
            MockServiceClient.Setup(s => s.ExecuteAsync<PartnerUser>(It.IsAny<IRestRequest>()))
               .ReturnsAsync(partnerObj)
                .Callback<IRestRequest>(r => Request = r);
            IPartnerUserRequest partnerUserObj = new PartnerUserRequest();
            var result = await PartnerService.AddPartnerUser("partnerId", partnerUserObj);
            Assert.Equal("/{partnerId}/users", Request.Resource);
            Assert.Equal(RestSharp.Method.POST, Request.Method);
            Assert.NotNull(result);
        }
    }
}
