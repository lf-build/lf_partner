﻿using LendFoundry.Partner.Api.Controllers;
using LendFoundry.Partner.Client;
using LendFoundry.Partner;
using Moq;
using Xunit;
using Microsoft.AspNet.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Partner.Api.Test
{
    public class PartnerApiTest
    {
        private static ApiController GetPartnerController(IPartnerService partnerService)
        {
            return new ApiController(partnerService);
        }


        [Fact]
        public async Task Controller_Add()
        {
            Mock<IPartnerService> PartnerService = new Mock<IPartnerService>();
            Partner partner = new Partner();
            PartnerService.Setup(s => s.Add(It.IsAny<IPartner>())).ReturnsAsync(partner);

            var getController = GetPartnerController(PartnerService.Object);

            var result = await getController.Add(partner);
            Assert.IsType<HttpOkObjectResult>(result);
            var application = ((HttpOkObjectResult)result).Value as IPartner;
            Assert.NotNull(application);
        }

        [Fact]
        public void Controller_AddContact()
        {
            Mock<IPartnerService> PartnerService = new Mock<IPartnerService>();
            Contact contact = new Contact();
            Partner partner = new Partner();
            PartnerService.Setup(s => s.AddContacts(It.IsAny<string>(), It.IsAny<IContact>())).ReturnsAsync(partner);

            var getController = GetPartnerController(PartnerService.Object);

            var result = getController.AddContact("partnerId", contact);
            Assert.IsType<HttpOkObjectResult>(result.Result);
           
        }

        [Fact]
        public async Task Controller_UpdateContact()
        {
            var response = new Mock<NoContentResult>();
            Mock<IPartnerService> PartnerService = new Mock<IPartnerService>();
            Contact contact = new Contact();
            Partner partner = new Partner();
            PartnerService.Setup(s => s.UpdateContacts(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<Contact>())).Returns(Task.FromResult(response.Object));

            var getController = GetPartnerController(PartnerService.Object);            
            Assert.IsType<NoContentResult>(await getController.UpdateContact("partnerId", "contactId", contact));

        }

        [Fact]
        public async Task Controller_Update()
        {
            var response = new Mock<NoContentResult>();
            Mock<IPartnerService> PartnerService = new Mock<IPartnerService>();
            Contact contact = new Contact();
            Partner partner = new Partner();
            PartnerService.Setup(s => s.Update(It.IsAny<string>(), It.IsAny<IPartner>())).Returns(Task.FromResult(response.Object));

            var getController = GetPartnerController(PartnerService.Object);
            Assert.IsType<NoContentResult>(await getController.Update("partnerId", partner));

        }

        //[Fact]
        //public async Task Controller_AssosiateUser()
        //{
        //    var response = new Mock<NoContentResult>();
        //    Mock<IPartnerService> PartnerService = new Mock<IPartnerService>();
        
        //    PartnerService.Setup(s => s.AssosiateUserToPartner(It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(response.Object));

        //    var getController = GetPartnerController(PartnerService.Object);
        //    Assert.IsType<NoContentResult>(await getController.AssosiateUser("partnerId", "test"));

        //}

        [Fact]
        public async Task Controller_UpdateActiveContact()
        {
            var response = new Mock<NoContentResult>();
            Mock<IPartnerService> PartnerService = new Mock<IPartnerService>();
            ContactUpdateRequest contact = new ContactUpdateRequest();
            Partner partner = new Partner();
            PartnerService.Setup(s => s.UpdateActiveContact(It.IsAny<string>(), It.IsAny<IContactUpdateRequest>())).Returns(Task.FromResult(response.Object));

            var getController = GetPartnerController(PartnerService.Object);
            Assert.IsType<NoContentResult>(await getController.UpdateActiveContact("partnerId", contact));

        }

        [Fact]
        public async Task Controller_Delete()
        {
            var response = new Mock<NoContentResult>();
            Mock<IPartnerService> PartnerService = new Mock<IPartnerService>();
            Contact contact = new Contact();
            Partner partner = new Partner();
            PartnerService.Setup(s => s.Remove(It.IsAny<string>())).Returns(Task.FromResult(response.Object));

            var getController = GetPartnerController(PartnerService.Object);
            Assert.IsType<NoContentResult>(await getController.Delete("partnerId"));

        }

        [Fact]
        public async Task Controller_DeleteContact()
        {

            var response = new Mock<NoContentResult>();
            Mock<IPartnerService> PartnerService = new Mock<IPartnerService>();
            Contact contact = new Contact();
            Partner partner = new Partner();
            PartnerService.Setup(s => s.DeleteContact(It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(response.Object));

            var getController = GetPartnerController(PartnerService.Object);         
             Assert.IsType <NoContentResult> (await getController.DeleteContact("partnerId", "contactId"));
        }

        [Fact]
        public async Task Controller_AttachUser()
        {
            var response = new Mock<NoContentResult>();
            Mock<IPartnerService> PartnerService = new Mock<IPartnerService>();
            Contact contact = new Contact();
            PartnerUserRequest partner = new PartnerUserRequest();
            PartnerService.Setup(s => s.AttachUser(It.IsAny<string>(), It.IsAny<string>(),It.IsAny<IPartnerUserRequest>())).Returns(Task.FromResult(response.Object));

            var getController = GetPartnerController(PartnerService.Object);
            
             Assert.IsType<NoContentResult>(await getController.AttachUser("partnerId", "contactId", partner));
        }

        [Fact]
        public async Task Controller_DetachUser()
        {
            var response = new Mock<NoContentResult>();
            Mock<IPartnerService> PartnerService = new Mock<IPartnerService>();
            Contact contact = new Contact();
            PartnerUserRequest partner = new PartnerUserRequest();
            PartnerService.Setup(s => s.DetachUser(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(response.Object)); 

            var getController = GetPartnerController(PartnerService.Object);
            
             Assert.IsType<NoContentResult>(await getController.DetachUser("partnerId", "contactId", "userId"));
        }

        [Fact]
        public async Task Controller_DeletePartnerUser()
        {
            var response = new Mock<NoContentResult>();
            Mock<IPartnerService> PartnerService = new Mock<IPartnerService>();
            Contact contact = new Contact();
            PartnerUserRequest partner = new PartnerUserRequest();
            PartnerService.Setup(s => s.DeletePartnerUser(It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(response.Object));

            var getController = GetPartnerController(PartnerService.Object);
            
              Assert.IsType<NoContentResult>(await getController.DeletePartnerUser("partnerId", "contactId"));
        }

        [Fact]
        public async Task Controller_GetAll()
        {
            Mock<IPartnerService> PartnerService = new Mock<IPartnerService>();
            Contact contact = new Contact();
            List<Partner> partner = new List<Partner>();
            PartnerService.Setup(s => s.GetAll()).ReturnsAsync(partner);

            var getController = GetPartnerController(PartnerService.Object);

            var result =await getController.GetAll();
            Assert.IsType<HttpOkObjectResult>(result);
          
        }

        [Fact]
        public async Task Controller_Get()
        {
            Mock<IPartnerService> PartnerService = new Mock<IPartnerService>();
            Partner partner = new Partner();
            PartnerService.Setup(s => s.Get(It.IsAny<string>())).ReturnsAsync(partner);

            var getController = GetPartnerController(PartnerService.Object);

            var result =await getController.Get("id");
            Assert.IsType<HttpOkObjectResult>(result);
         
        }

        [Fact]
        public async Task Controller_AddPartnerUser()
        {
            Mock<IPartnerService> PartnerService = new Mock<IPartnerService>();
            PartnerUserRequest partner = new PartnerUserRequest();
            PartnerUser partnerUser = new PartnerUser();
            PartnerService.Setup(s => s.AddPartnerUser(It.IsAny<string>(), It.IsAny<IPartnerUserRequest>())).ReturnsAsync(partnerUser);

            var getController = GetPartnerController(PartnerService.Object);

           var result = await getController.AddPartnerUser("id", partner);

            var application = ((HttpOkObjectResult)result).Value as IPartnerUser;
             Assert.NotNull(application);
        }
    }
}
