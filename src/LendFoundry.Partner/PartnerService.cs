﻿using LendFoundry.StatusManagement;
using LendFoundry.EventHub;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.NumberGenerator;
using LendFoundry.Security.Identity;
using LendFoundry.Security.Identity.Client;
using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Partner
{
    public class PartnerService : IPartnerService
    {
        #region Constructor
        public PartnerService(IPartnerRepository partnerRepository, IEventHubClient eventHub, IPartnerUserRepository partnerUserRepository, LendFoundry.Security.Identity.Client.IIdentityService identityService, PartnerConfigurations partnerConfigurations, ITenantTime tenantTime, ITokenHandler tokenParser, ITokenReader tokenReader, IEntityStatusService statusManagmentService, IGeneratorService applicationNumberGenerator, ILogger logger)
        {
            PartnerRepository = partnerRepository;
            EventHub = eventHub;
            PartnerUserRepository = partnerUserRepository;
            IdentityService = identityService;
            PartnerConfigurations = partnerConfigurations;
            TenantTime = tenantTime;
            TokenParser = tokenParser;
            TokenReader = tokenReader;
            StatusManagmentService = statusManagmentService;
            ApplicationNumberGenerator = applicationNumberGenerator;
            Logger = logger;

        }
        #endregion

        #region Private Variables
        private IPartnerRepository PartnerRepository { get; }
        private IEventHubClient EventHub { get; }
        private IPartnerUserRepository PartnerUserRepository { get; }
        private LendFoundry.Security.Identity.Client.IIdentityService IdentityService { get; }
        private PartnerConfigurations PartnerConfigurations { get; }
        private IGeneratorService ApplicationNumberGenerator { get; }

        private ITenantTime TenantTime { get; }

        private ITokenHandler TokenParser { get; }

        private ITokenReader TokenReader { get; }

        private IEntityStatusService StatusManagmentService { get; set; }
        private ILogger Logger { get; }
        #endregion

        public async Task<IPartner> Add(IPartner partner)
        {
            try
            {
                Logger.Info("Started Execution for Add Partner Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                EnsureDataIsValid(partner);
                if (partner.Contacts != null && partner.Contacts.Any())
                {
                    foreach (var Contact in partner.Contacts)
                    {
                        Contact.ContactId = string.IsNullOrWhiteSpace(Contact.ContactId) ? GenerateUniqueId() : Contact.ContactId;
                    }
                }

                // if has bank account apply needed treatments
                if (partner.BankAccounts != null && partner.BankAccounts.Any())
                {
                    EnsureBankAccountIsValid(partner.BankAccounts);
                    SetBankAccountId(partner.BankAccounts);
                    EnsureNoDuplicatedBankAccount(partner.BankAccounts);

                    // if not informed first bank must to be assumed as primary
                    if (partner.BankAccounts.TrueForAll(b => b.IsPrimary == false))
                        partner.BankAccounts.First().IsPrimary = true;
                }
                else
                    partner.BankAccounts = new List<IBankAccount>();

                partner.Status = PartnerConfigurations.InitialEnrollmentStatusCode;
                partner.StatusChangeDate = new TimeBucket(TenantTime.Now);
                partner.SubmittedDate = new TimeBucket(TenantTime.Now);
                partner.InCorporationDate = new TimeBucket(TenantTime.Now);
                partner.PartnerId = ApplicationNumberGenerator.TakeNext("partner").Result;

                if (string.IsNullOrWhiteSpace(partner.PartnerId))
                    throw new Exception("Unable to generate partner number");

                await Task.Run(() => PartnerRepository.Add(partner));
                await StatusManagmentService.InitiateSubWorkFlow("partner", partner.PartnerId, PartnerConfigurations.StatusWorkFlow);
                await StatusManagmentService.ChangeStatus("partner", partner.PartnerId, PartnerConfigurations.StatusWorkFlow, PartnerConfigurations.InitialEnrollmentStatusCode, new RequestModel());

                await EventHub.Publish(new Events.PartnerAdded(partner));

                Logger.Info("Completed Execution for Add Partner Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                return partner;
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing Add Partner Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);
                throw;
            }
        }
        public async Task Remove(string partnerId)
        {
            if (string.IsNullOrWhiteSpace(partnerId))
                throw new InvalidArgumentException("Partner Id can not be empty");
            try
            {
                Logger.Info("Started Execution for Remove Partner Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                var partner = await Get(partnerId);
                if (partner == null)
                    throw new NotFoundException($"Partner id {partnerId} can not be found");
                await Task.Run(() => PartnerRepository.Remove(partner));
                await EventHub.Publish(new Events.PartnerRemoved(partner));
                Logger.Info("Completed Execution for Remove Partner Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");

            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing Remove Partner Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);
                throw;
            }
        }
        public async Task<IPartner> Get(string partnerId)
        {
            if (string.IsNullOrWhiteSpace(partnerId))
                throw new InvalidArgumentException("Partner Id can not be empty");
            try
            {
                Logger.Info("Started Execution for Get Partner Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                var result = await PartnerRepository.GetByPartnerId(partnerId);
                if (result == null)
                    throw new NotFoundException($"Partner Id {partnerId} can not be found");
                Logger.Info("Completed Execution for Get Partner Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                return result;
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing Get Partner Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);
                throw;
            }
        }
        public async Task<IPartner> AddContacts(string partnerId, IContact contact)
        {
            if (string.IsNullOrWhiteSpace(partnerId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(partnerId));

            if (contact == null)
                throw new ArgumentException($"{nameof(contact)} is mandatory");
            try
            {
                Logger.Info("Started Execution for AddContacts Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                EnsureDataIsValid(contact);

                var partner = await PartnerRepository.GetByPartnerId(partnerId);

                if (partner == null)
                    throw new NotFoundException($"Partner {partnerId} not found");


                var lastContact = partner.Contacts != null ? partner.Contacts.ToList() ?? new List<IContact>() : new List<IContact>();
                if (contact.IsPrimary)
                    lastContact.ForEach(a => { a.IsPrimary = false; });

                contact.ContactId = GenerateUniqueId();
                lastContact.Add(contact);

                partner.Contacts = lastContact;
                await PartnerRepository.UpdateContacts(partnerId, lastContact);
                await EventHub.Publish(new Events.PartnerUpdated(partner));
                Logger.Info("Completed Execution for AddContacts Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                return partner;
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing AddContacts Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);
                throw;
            }
        }
        public async Task<IPartner> AddUser(string partnerId, IContactUserRequest contactUserRequest)
        {
            if (string.IsNullOrWhiteSpace(partnerId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(partnerId));

            if (contactUserRequest == null)
                throw new ArgumentException($"{nameof(contactUserRequest)} is mandatory");
            try
            {
                Logger.Info("Started Execution for AddUser Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                EnsureDataIsValid(contactUserRequest);

                //1)  Create User 
                var userid = await CreateUser(contactUserRequest.UserName, contactUserRequest.Password, contactUserRequest.Email, contactUserRequest.Roles);

                //2) Associate that user to partnerId
                AssosiateUserToPartner(partnerId, userid).Wait();

                var partner = await PartnerRepository.GetByPartnerId(partnerId);

                if (partner == null)
                    throw new NotFoundException($"Partner {partnerId} not found");

                //3) Create Contact
                IContact contact = new Contact();
                contact.Email = contactUserRequest.Email;
                contact.FirstName = contactUserRequest.FirstName;
                contact.LastName = contactUserRequest.LastName;
                contact.IsPrimary = contactUserRequest.IsPrimary;

                var lastContact = partner.Contacts != null ? partner.Contacts.ToList() ?? new List<IContact>() : new List<IContact>();
                if (contact.IsPrimary)
                    lastContact.ForEach(a => { a.IsPrimary = false; });

                contact.ContactId = GenerateUniqueId();
                //4) Associate User to Contact
                contact.UserId = userid;
                lastContact.Add(contact);

                partner.Contacts = lastContact;
                await PartnerRepository.UpdateContacts(partnerId, lastContact);
                await EventHub.Publish(new Events.PartnerUpdated(partner));
                Logger.Info("Completed Execution for AddUser Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                return partner;
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing AddUser Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);
                throw;
            }
        }
        public async Task UpdateContacts(string partnerId, string contactId, IContact contact)
        {
            if (string.IsNullOrWhiteSpace(partnerId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(partnerId));

            if (string.IsNullOrWhiteSpace(contactId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(contactId));

            if (contact == null)
                throw new ArgumentException($"{nameof(contact)} is mandatory");
            try
            {
                Logger.Info("Started Execution for UpdateContacts Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                EnsureDataIsValid(contact);

                var partner = await PartnerRepository.GetByPartnerId(partnerId);

                if (partner == null)
                    throw new NotFoundException($"Partner {partner} not found");

                var contacts = partner.Contacts ?? new List<IContact>();
                if (contact.IsPrimary)
                    contacts.ForEach(a => { a.IsPrimary = false; });

                contact.ContactId = contactId;
                contacts = contacts.Select(adr => adr.ContactId == contactId ? contact : adr).ToList();

                partner.Contacts = contacts;
                await PartnerRepository.UpdateContacts(partnerId, contacts);
                await EventHub.Publish(new Events.PartnerUpdated(partner));
                Logger.Info("Completed Execution for UpdateContacts Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");

            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing UpdateContacts Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);
                throw;
            }

        }
        public async Task DeleteContact(string partnerId, string contactId)
        {
            if (string.IsNullOrWhiteSpace(partnerId))
                throw new ArgumentException("Argument is null or whitespace", nameof(partnerId));

            if (string.IsNullOrWhiteSpace(contactId))
                throw new ArgumentException("Argument is null or whitespace", nameof(contactId));

            var partner = await PartnerRepository.GetByPartnerId(partnerId);
            if (partner == null)
                throw new NotFoundException($"Applicant {partnerId} not found");
            try
            {
                Logger.Info("Started Execution for DeleteContact Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                var contacts = partner.Contacts ?? new List<IContact>();
                if (contacts.Any(a => a.ContactId == contactId) == false)
                    throw new NotFoundException($"Contact with {contactId} for Partner {partnerId} not found");
                var DeletedContact = contacts.First(a => a.ContactId == contactId);
                if (DeletedContact.IsPrimary)
                    throw new InvalidOperationException("Default Contact can not be deleted");

                contacts.Remove(contacts.First(a => a.ContactId == contactId));
                await PartnerRepository.UpdateContacts(partnerId, contacts);
                await EventHub.Publish(new Events.PartnerUpdated(partner));
                Logger.Info("Completed Execution for DeleteContact Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");

            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing DeleteContact Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);
                throw;
            }
        }
        public async Task AttachUser(string partnerId, string contactId, IPartnerUserRequest partnerUserRequest)
        {

            if (string.IsNullOrWhiteSpace(partnerId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(partnerId));

            if (string.IsNullOrWhiteSpace(contactId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(contactId));

            if (string.IsNullOrWhiteSpace(partnerUserRequest.UserName))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(partnerUserRequest.UserName));

            if (string.IsNullOrWhiteSpace(partnerUserRequest.Password))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(partnerUserRequest.Password));

            var partner = await PartnerRepository.GetByPartnerId(partnerId);

            if (partner == null)
                throw new NotFoundException($"Partner {partner} not found");

            try
            {
                Logger.Info("Started Execution for AttachUser Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                var contacts = partner.Contacts ?? new List<IContact>();
                if (contacts.Any(a => a.ContactId == contactId) == false)
                    throw new NotFoundException($"Contact with {contactId} for Partner {partnerId} not found");
                var attachedContact = contacts.First(a => a.ContactId == contactId);

                var userid = await CreateUser(partnerUserRequest.UserName, partnerUserRequest.Password, attachedContact.Email, null);
                attachedContact.UserId = userid;
                AssosiateUserToPartner(partnerId, userid).Wait();


                partner.Contacts = contacts;
                await PartnerRepository.UpdateContacts(partnerId, contacts);
                await EventHub.Publish(new Events.PartnerUpdated(partner));
                Logger.Info("Completed Execution for AttachUser Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");

            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing AttachUser Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);
                throw;
            }
        }
        public async Task ChangeStatus(string partnerId, string statusCode)
        {
            if (string.IsNullOrWhiteSpace(partnerId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(partnerId));

            if (string.IsNullOrWhiteSpace(statusCode))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(statusCode));

            var partner = await PartnerRepository.GetByPartnerId(partnerId);

            if (partner == null)
                throw new NotFoundException($"Partner {partner} not found");

            try
            {
                Logger.Info("Started Execution for ChangeStatus Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                await StatusManagmentService.ChangeStatus("partner", partner.PartnerId, PartnerConfigurations.StatusWorkFlow, statusCode, new RequestModel());

                partner.Status = statusCode;
                partner.StatusChangeDate = new TimeBucket(TenantTime.Now);
                partner.StatusChangedBy = EnsureCurrentUser();

                await Task.Run(() => PartnerRepository.Update(partnerId, partner));

                await EventHub.Publish(new Events.PartnerUpdated(partner));
                Logger.Info("Completed Execution for ChangeStatus Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing ChangeStatus Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);
                throw;
            }
        }
        public async Task DetachUser(string partnerId, string contactId, string userId)
        {
            if (string.IsNullOrWhiteSpace(partnerId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(partnerId));

            if (string.IsNullOrWhiteSpace(contactId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(contactId));

            if (string.IsNullOrWhiteSpace(userId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(userId));

            var partner = await PartnerRepository.GetByPartnerId(partnerId);

            if (partner == null)
                throw new NotFoundException($"Partner {partner} not found");
            try
            {
                Logger.Info("Started Execution for DetachUser Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                var contacts = partner.Contacts ?? new List<IContact>();
                if (contacts.Any(a => a.ContactId == contactId) == false)
                    throw new NotFoundException($"Contact with {contactId} for Partner {partnerId} not found");
                var detachContact = contacts.First(a => a.ContactId == contactId);
                detachContact.UserId = null;

                partner.Contacts = contacts;
                await PartnerRepository.UpdateContacts(partnerId, contacts);
                await DeletePartnerUser(partnerId, userId);
                await EventHub.Publish(new Events.PartnerUpdated(partner));
                Logger.Info("Completed Execution for DetachUser Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");

            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing DetachUser Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);
                throw;
            }
        }
        public async Task Update(string partnerId, IUpdatePartner partner)
        {
            if (string.IsNullOrWhiteSpace(partnerId))
                throw new InvalidArgumentException($"{nameof(partnerId)} is mandatory");
            try
            {
                Logger.Info("Started Execution for Update Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                var dbPartner = await Get(partnerId);
                if (dbPartner == null)
                    throw new NotFoundException($"Partner Id {partnerId} can not be found");
                EnsureDataIsValid(partner);
                dbPartner.InCorporationDate = new TimeBucket(partner.InCorporationDate);
                dbPartner.Name = partner.Name;
                dbPartner.NickName = partner.NickName;
                dbPartner.Notes = partner.Notes;
                dbPartner.PartnerId = partner.PartnerId;
                dbPartner.PartnerType = partner.PartnerType;
                dbPartner.PrincipalOwner = partner.PrincipalOwner;
                dbPartner.Status = partner.Status;
                dbPartner.StatusChangedBy = partner.StatusChangedBy;
                dbPartner.TenantId = partner.TenantId;
                dbPartner.WebSite = partner.WebSite;
                dbPartner.EIN = partner.EIN;
                dbPartner.DBA = partner.DBA;
                dbPartner.Contacts = partner.Contacts;
                dbPartner.BankAccounts = partner.BankAccounts;
                dbPartner.Address = partner.Address;
                await Task.Run(() => PartnerRepository.Update(partnerId, dbPartner));
                await EventHub.Publish(new Events.PartnerUpdated(dbPartner));
                Logger.Info("Completed Execution for Update Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");

            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing Update Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);
                throw;
            }
        }


        public async Task<IEnumerable<IPartner>> GetAll()
        {
            try
            {
                return await PartnerRepository.GetAll();
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing GetAll Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);
                throw;
            }
        }
        public async Task UpdateActiveContact(string partnerId, IContactUpdateRequest contactUpdateRequest)
        {
            if (partnerId == null) throw new ArgumentNullException(nameof(partnerId));
            if (contactUpdateRequest == null) throw new ArgumentNullException(nameof(contactUpdateRequest));
            if (string.IsNullOrWhiteSpace(partnerId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(partnerId));

            if (string.IsNullOrWhiteSpace(contactUpdateRequest.FirstName))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(contactUpdateRequest.FirstName));

            if (string.IsNullOrWhiteSpace(contactUpdateRequest.LastName))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(contactUpdateRequest.LastName));

            if (string.IsNullOrWhiteSpace(contactUpdateRequest.JobTitle))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(contactUpdateRequest.JobTitle));

            if (string.IsNullOrWhiteSpace(contactUpdateRequest.Email))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(contactUpdateRequest.Email));

            if (string.IsNullOrWhiteSpace(contactUpdateRequest.PhoneNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(contactUpdateRequest.PhoneNumber));
            try
            {
                Logger.Info("Started Execution for UpdateActiveContact Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                var partner = await PartnerRepository.GetByPartnerId(partnerId);

                if (partner == null)
                    throw new NotFoundException($"Partner {partnerId} cannot be found");

                var contacts = partner.Contacts.ToList();
                var activeContact = contacts.FirstOrDefault(p => p.IsPrimary);

                if (activeContact == null)
                    throw new NotFoundException("No Active Contact found");

                activeContact.FirstName = contactUpdateRequest.FirstName;
                activeContact.LastName = contactUpdateRequest.LastName;
                activeContact.JobTitle = contactUpdateRequest.JobTitle;
                activeContact.Email = contactUpdateRequest.Email;
                activeContact.PhoneNumber = contactUpdateRequest.PhoneNumber;
                activeContact.PhoneExt = contactUpdateRequest.PhoneExt;

                await PartnerRepository.UpdateContacts(partnerId, contacts);
                Logger.Info("Completed Execution for UpdateActiveContact Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");

            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing UpdateActiveContact Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);
                throw;
            }
        }
        public async Task DeletePartnerUser(string partnerId, string userId)
        {
            if (string.IsNullOrWhiteSpace(partnerId))
                throw new InvalidArgumentException($"{nameof(partnerId)} is mandatory");

            if (string.IsNullOrWhiteSpace(userId))
                throw new InvalidArgumentException($"{nameof(userId)} is mandatory");
            try
            {
                Logger.Info("Started Execution for DeletePartnerUser Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                var partnerUser = await GetPartnerUser(partnerId, userId);
                if (partnerUser == null)
                    throw new NotFoundException($"PartnerId and UserId {nameof(partnerId)} is not Found");

                Logger.Info("Completed Execution for DeletePartnerUser Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                await Task.Run(() => PartnerUserRepository.Remove(partnerUser));
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing DeletePartnerUser Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);
                throw;
            }
        }
        public async Task<IPartnerUser> GetPartnerUser(string partnerId, string userId)
        {
            try
            {
                Logger.Info("Started Execution for GetPartnerUser Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                var result = await PartnerUserRepository.GetByPartnerandUserId(partnerId, userId);
                if (result == null)
                    throw new NotFoundException($"Partner id {partnerId} and UserId {userId} cannot be found");

                Logger.Info("Completed Execution for GetPartnerUser Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                return result;
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing GetPartnerUser Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);
                throw;
            }
        }
        public async Task<IPartnerUser> AddPartnerUser(string partnerId, IPartnerUserRequest partnerUserRequest)
        {
            if (string.IsNullOrWhiteSpace(partnerId))
                throw new InvalidArgumentException($"{nameof(partnerId)} is mandatory");

            try
            {
                Logger.Info("Started Execution for AddPartnerUser Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                var result = await PartnerRepository.Any(partnerId);
                if (!result)
                    throw new NotFoundException($"Partner id {partnerId} cannot be found");

                if (partnerUserRequest == null)
                    throw new InvalidArgumentException($"{nameof(partnerUserRequest)} is mandatory");

                if (string.IsNullOrWhiteSpace(partnerUserRequest.UserName))
                    throw new InvalidOperationException($"UserName is {partnerUserRequest.UserName} mandatory");

                if (string.IsNullOrWhiteSpace(partnerUserRequest.Password))
                    throw new InvalidOperationException($"Password is {partnerUserRequest.Password} mandatory");

                if (string.IsNullOrWhiteSpace(partnerUserRequest.Email))
                    throw new InvalidOperationException($"Email is {partnerUserRequest.Email} mandatory");

                var userid = await CreateUser(partnerUserRequest.UserName, partnerUserRequest.Password, partnerUserRequest.Email, null);
                AssosiateUserToPartner(partnerId, userid).Wait();

                IPartnerUser partnerUser = new PartnerUser();
                partnerUser.PartnerId = partnerId;
                partnerUser.UserId = userid;

                Logger.Info("Completed Execution for AddPartnerUser Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                return partnerUser;
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing AddPartnerUser Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);
                throw;
            }
        }
        private async Task<string> CreateUser(string userName, string password, string emailAddress, List<string> Roles)
        {
            try
            {
                Logger.Info("Started Execution for CreateUser Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                IUser user = new User()
                {
                    Email = emailAddress,
                    Name = userName,
                    Username = userName,
                    Password = password,
                    Roles = Roles != null ? Roles.ToArray() : PartnerConfigurations.DefaultRoles
                };

                var userInfo = await IdentityService.CreateUser(user);
                if (string.IsNullOrEmpty(userInfo.Id.ToString()))
                    throw new UnableToCreateUserException();

                Logger.Info("Completed Execution for CreateUser Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                return userInfo.Id.ToString();
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing CreateUser Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);
                throw;
            }
        }
        public async Task<IBankAccount> AddBank(string partnerId, IBankAccount bankAccount)

        {
            if (string.IsNullOrWhiteSpace(partnerId))
                throw new InvalidArgumentException($"{nameof(partnerId)} is mandatory");

            if (bankAccount == null)
                throw new InvalidArgumentException($"{nameof(bankAccount)} is mandatory");
            try
            {
                Logger.Info("Started Execution for AddBank Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                // applies bank entry validation
                var banks = new[] { bankAccount }.ToList();
                EnsureBankAccountIsValid(banks);
                SetBankAccountId(banks);

                // cancel operation in case of existing bank w/ same keys
                var partner = await Get(partnerId);
                if (partner.BankAccounts == null)
                    partner.BankAccounts = new List<IBankAccount>();

                if (partner.BankAccounts.Any(b => b.Id == bankAccount.Id))
                    throw new InvalidOperationException($"Bank AccountNumber {bankAccount.AccountNumber} and RoutingNumber {bankAccount.RoutingNumber} already exists");

                // set this bank as primary if informed
                if (bankAccount.IsPrimary)
                    partner.BankAccounts.ForEach(b => { b.IsPrimary = false; });

                // add new bank to merchant bank list and save it
                partner.BankAccounts.Add(bankAccount);
                await EventHub.Publish(new Events.PartnerUpdated(partner));

                EnsurePrimaryBankAccount(partner.BankAccounts);
                PartnerRepository.Update(partner);
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing AddBank Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);
                throw;
            }
            Logger.Info("Completed Execution for AddBank Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
            return bankAccount;
        }
        public async Task<IBankAccount> UpdateBank(string partnerId, string bankId, IBankAccount bankAccount)
        {
            if (string.IsNullOrWhiteSpace(partnerId))
                throw new InvalidArgumentException($"{nameof(partnerId)} is mandatory");

            if (string.IsNullOrWhiteSpace(bankId))
                throw new InvalidArgumentException($"{nameof(bankId)} is mandatory");
            try
            {
                Logger.Info("Started Execution for UpdateBank Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                // applies bank entry validation
                var banks = new[] { bankAccount }.ToList();
                EnsureBankAccountIsValid(banks);
                SetBankAccountId(banks);

                // stop process in case of not found bank to update
                var partner = await Get(partnerId);
                if (!partner.BankAccounts.Any(b => b.Id == bankId))
                    throw new NotFoundException($"Bank Account {bankId} could not be found");

                // The code below was commented to make this method works to Front-end team!
                // TODO: Make more tests with this code and validation below! - Handerson + Marcio (2016-09-19)
                // ensure if already there's a bank with same entries
                //if (merchant.BankAccounts.Any(b => b.Id == bankAccount.Id && b.AccountType == bankAccount.AccountType))
                //    throw new InvalidOperationException($"Bank AccountNumber {bankAccount.AccountNumber}, RoutingNumber {bankAccount.RoutingNumber} and Type {bankAccount.AccountType} already exists");

                // set this bank as primary if informed
                if (bankAccount.IsPrimary)
                    partner.BankAccounts.ForEach(b => { b.IsPrimary = false; });

                // update internal bank reference
                partner.BankAccounts = partner.BankAccounts.Select(bank =>
                {
                    return bank.Id == bankId ? bankAccount : bank;
                }).ToList();

                // ensure after performed update there's now invalid entries
                if (partner.BankAccounts.Count(b => b.Id == bankAccount.Id) != 1)
                    throw new InvalidOperationException($"Bank AccountNumber {bankAccount.AccountNumber}, RoutingNumber {bankAccount.RoutingNumber} already exists");

                EnsurePrimaryBankAccount(partner.BankAccounts);
                PartnerRepository.Update(partner);
                await EventHub.Publish(new Events.PartnerUpdated(partner));
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing UpdateBank Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);
                throw;
            }
            Logger.Info("Completed Execution for UpdateBank Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
            return bankAccount;
        }
        public async Task SetBankAsPrimary(string partnerId, string bankId)
        {
            if (string.IsNullOrWhiteSpace(partnerId))
                throw new InvalidArgumentException($"{nameof(partnerId)} is mandatory");

            if (string.IsNullOrWhiteSpace(bankId))
                throw new InvalidArgumentException($"{nameof(bankId)} is mandatory");
            try
            {
                Logger.Info("Started Execution for SetBankAsPrimary Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                // stop process in case of not found bank to update
                var partner = await Get(partnerId);
                if (!partner.BankAccounts.Any(b => b.Id == bankId))
                    throw new NotFoundException($"Bank Account {bankId} could not be found");

                // update internal bank reference
                partner.BankAccounts = partner.BankAccounts.Select(bank =>
                {
                    bank.IsPrimary = bank.Id == bankId;
                    return bank;
                }).ToList();

                PartnerRepository.Update(partner);
                await EventHub.Publish(new Events.PartnerUpdated(partner));
                Logger.Info("Completed Execution for SetBankAsPrimary Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");

            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing SetBankAsPrimary Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);
                throw;
            }
        }
        public async Task SetContactAsPrimary(string partnerId, string contactId)
        {
            if (string.IsNullOrWhiteSpace(partnerId))
                throw new InvalidArgumentException($"{nameof(partnerId)} is mandatory");

            if (string.IsNullOrWhiteSpace(contactId))
                throw new InvalidArgumentException($"{nameof(contactId)} is mandatory");

            try
            {
                var partner = await Get(partnerId);
                if (!partner.Contacts.Any(b => b.ContactId == contactId))
                    throw new NotFoundException($"Contact {contactId} could not be found");


                partner.Contacts = partner.Contacts.Select(b =>
                {
                    b.IsPrimary = b.ContactId == contactId;
                    return b;
                }).ToList();

                PartnerRepository.Update(partner);
                await EventHub.Publish(new Events.PartnerUpdated(partner));
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing SetContactAsPrimary Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);
                throw;
            }
        }
        public async Task DeleteBank(string partnerId, string bankId)
        {
            if (string.IsNullOrWhiteSpace(partnerId))
                throw new InvalidArgumentException($"{nameof(partnerId)} is mandatory");

            if (string.IsNullOrWhiteSpace(bankId))
                throw new InvalidArgumentException($"{nameof(bankId)} is mandatory");
            try
            {
                Logger.Info("Started Execution for DeleteBank Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                // stop process in case of not found bank to update
                var partner = await Get(partnerId);
                if (!partner.BankAccounts.Any(b => b.Id == bankId))
                    throw new NotFoundException($"Bank Account id {bankId} could not be found");

                // primary bank account cannot be deleted
                if (partner.BankAccounts.First(b => b.Id == bankId).IsPrimary)
                    throw new InvalidOperationException("Primary bank account cannot be deleted");

                // remove from list informed bank account
                partner.BankAccounts = partner.BankAccounts.Where(b => b.Id != bankId).ToList();

                EnsurePrimaryBankAccount(partner.BankAccounts);
                PartnerRepository.Update(partner);
                await EventHub.Publish(new Events.PartnerUpdated(partner));
                Logger.Info("Completed Execution for DeleteBank Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");

            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing DeleteBank Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);
                throw;
            }
        }
        public async Task<IEnumerable<IBankAccount>> GetAllBanks(string partnerId)
        {
            if (string.IsNullOrWhiteSpace(partnerId))
                throw new InvalidArgumentException($"{nameof(partnerId)} is mandatory");
            try
            {
                Logger.Info("Started Execution for GetAllBanks Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                var partner = await Get(partnerId);
                if (partner == null)
                    throw new NotFoundException($"Partner {partnerId} could not be found");

                var banks = partner?.BankAccounts;
                if (banks == null)
                    throw new NotFoundException($"No bank accounts found to merchant {partnerId}");

                Logger.Info("Completed Execution for GetAllBanks Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                return banks;
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing GetAllBanks Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);
                throw;
            }
        }
        public async Task<IBankAccount> GetBank(string partnerId, string bankId)
        {
            if (string.IsNullOrWhiteSpace(partnerId))
                throw new InvalidArgumentException($"{nameof(partnerId)} is mandatory");

            if (string.IsNullOrWhiteSpace(bankId))
                throw new InvalidArgumentException($"{nameof(bankId)} is mandatory");
            try
            {
                Logger.Info("Started Execution for GetBank Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                var partner = await Get(partnerId);
                if (partner == null)
                    throw new NotFoundException($"partner {partnerId} could not be found");

                var bank = partner?.BankAccounts.FirstOrDefault(b => b.Id == bankId);
                if (bank == null)
                    throw new NotFoundException($"Bank account {bankId} could not be found");

                Logger.Info("Completed Execution for GetBank Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                return bank;
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing GetBank Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);
                throw;
            }
        }
        public async Task<List<IContact>> GetContacts(string partnerId)
        {
            if (string.IsNullOrWhiteSpace(partnerId))
                throw new InvalidArgumentException($"{nameof(partnerId)} is mandatory");
            try
            {
                Logger.Info("Started Execution for GetContacts Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                var partner = await Get(partnerId);
                if (partner == null)
                    throw new NotFoundException($"partner {partnerId} could not be found");

                if (partner.Contacts == null)
                    throw new NotFoundException($"There is no contacts available for {partnerId}");

                Logger.Info("Completed Execution for GetContacts Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                return partner.Contacts;
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing GetContacts Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);
                throw;
            }

        }
        public async Task<List<IUserInfo>> GetPartnerUsers(string partnerId)
        {
            List<IUserInfo> usermodelList = new List<IUserInfo>();
            if (string.IsNullOrWhiteSpace(partnerId))
                throw new InvalidArgumentException($"{nameof(partnerId)} is mandatory");
            try
            {
                Logger.Info("Started Execution for GetPartnerUsers Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                var partner = await PartnerUserRepository.GetByPartnerId(partnerId);
                if (partner == null)
                    throw new NotFoundException($"partner {partnerId}  does not have any users.");

                foreach (var item in partner)
                {
                    var userDetails = await IdentityService.GetUserById(item.UserId);

                    if (userDetails != null)
                        usermodelList.Add(userDetails);
                }
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing GetPartnerUsers Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);
                throw;
            }
            Logger.Info("Completed Execution for Login Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
            return usermodelList;
        }

        public async Task<IPartnerUser> GetByUserId(string userId)
        {

            if (string.IsNullOrWhiteSpace(userId))
                throw new InvalidArgumentException($"{nameof(userId)} is mandatory");
            try
            {
                return await PartnerUserRepository.GetByUserId(userId);
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing GetByUserId Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);
                throw;
            }
        }

        public async Task AssosiateUserToPartner(string partnerId, string userId)
        {
            if (string.IsNullOrWhiteSpace(partnerId))
                throw new InvalidArgumentException($"{nameof(partnerId)} is mandatory");

            if (string.IsNullOrWhiteSpace(userId))
                throw new InvalidArgumentException($"{nameof(userId)} is mandatory");

            var result = await PartnerRepository.Any(partnerId);
            if (!result)
                throw new NotFoundException($"Partner id {partnerId} cannot be found");
            try
            {
                Logger.Info("Started Execution for Login Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                IPartnerUser partnerUser = new PartnerUser()
                {
                    PartnerId = partnerId,
                    UserId = userId
                };
                var partnerUsers = await PartnerUserRepository.GetByUserId(userId);
                if (partnerUsers != null)
                    throw new InvalidOperationException($"User id {userId} already associated with the PartnerId {partnerUsers.PartnerId}.");

                Logger.Info("Completed Execution for Login Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                await Task.Run(() => PartnerUserRepository.Add(partnerUser));
            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing AssosiateUserToPartner Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);
                throw;
            }
        }
        private void SetBankAccountId(List<IBankAccount> bankAccounts)
        {
            bankAccounts.ForEach(bankAccount => { bankAccount.Id = GenerateUniqueId(); });
        }
        public async Task<LoginResponseModel> Login(ILoginRequestModel login)
        {
            if (login == null)
                throw new InvalidArgumentException($"{nameof(login)} is mandatory");
            try
            {
                Logger.Info("Started Execution for Login Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                LoginRequest request = new LoginRequest();
                request.Password = login.Password;
                request.Portal = login.Portal;
                request.Username = login.Username;
                var result = await IdentityService.Login(request);
                if (result == null)
                    throw new NotFoundException($"Invalid Credential");

                var partnerUsers = await PartnerUserRepository.GetByUserId(result.UserId);

                LoginResponseModel loginResponse = new LoginResponseModel();
                loginResponse.UserId = result.UserId;
                loginResponse.Token = result.Token;
                loginResponse.PartnerId = partnerUsers != null ? partnerUsers.PartnerId : string.Empty;
                Logger.Info("Completed Execution for Login Request Info: Date & Time:" + TenantTime.Now + " Service: Partner");
                return loginResponse;

            }
            catch (Exception exception)
            {
                Logger.Error("Error While Processing Login Date & Time:" + TenantTime.Now + "Service: Partner" + "Exception" + exception.Message);

                throw;
            }
        }
        private void EnsureDataIsValid(IPartner partner)
        {
            if (partner == null)
                throw new InvalidArgumentException("Partner can not be null");
            if (string.IsNullOrWhiteSpace(partner.Name))
                throw new InvalidArgumentException($"{nameof(partner.Name)} is mandatory");
            if (partner.Address == null)
                throw new InvalidArgumentException($"{nameof(partner.Address)} is mandatory");
            if (string.IsNullOrWhiteSpace(partner.Address.Line1))
                throw new InvalidArgumentException($"{nameof(partner.Address.Line1)} is mandatory");

            //if (partner.Contacts == null || !partner.Contacts.Any())
            //    throw new ArgumentNullException($"{nameof(partner.Contacts)} is mandatory");


        }
        private void EnsureDataIsValid(IUpdatePartner partner)
        {
            if (partner == null)
                throw new InvalidArgumentException("Partner can not be null");
            if (string.IsNullOrWhiteSpace(partner.Name))
                throw new InvalidArgumentException($"{nameof(partner.Name)} is mandatory");
            if (partner.Address == null)
                throw new InvalidArgumentException($"{nameof(partner.Address)} is mandatory");
            if (string.IsNullOrWhiteSpace(partner.Address.Line1))
                throw new InvalidArgumentException($"{nameof(partner.Address.Line1)} is mandatory");

            //if (partner.Contacts == null || !partner.Contacts.Any())
            //    throw new ArgumentNullException($"{nameof(partner.Contacts)} is mandatory");


        }
        private void EnsurePrimaryBankAccount(List<IBankAccount> bankAccounts)
        {
            // if not informed first bank must to be assumed as primary
            if (bankAccounts.Any() && bankAccounts.TrueForAll(b => b.IsPrimary == false))
                bankAccounts.First().IsPrimary = true;
        }
        private void EnsureDataIsValid(IContact contact)
        {
            if (string.IsNullOrWhiteSpace(contact.FirstName))
                throw new ArgumentNullException($"Partner {nameof(contact.FirstName)} is mandatory");

            if (string.IsNullOrWhiteSpace(contact.Email))
                throw new ArgumentNullException($"Partner {nameof(contact.Email)} is mandatory");
        }
        private void EnsureDataIsValid(IContactUserRequest contactUserRequest)
        {
            if (string.IsNullOrWhiteSpace(contactUserRequest.FirstName))
                throw new ArgumentNullException($"Partner {nameof(contactUserRequest.FirstName)} is mandatory");

            if (string.IsNullOrWhiteSpace(contactUserRequest.Email))
                throw new ArgumentNullException($"Partner {nameof(contactUserRequest.Email)} is mandatory");

            if (string.IsNullOrWhiteSpace(contactUserRequest.UserName))
                throw new ArgumentNullException($"Partner {nameof(contactUserRequest.UserName)} is mandatory");

            if (string.IsNullOrWhiteSpace(contactUserRequest.Password))
                throw new ArgumentNullException($"Partner {nameof(contactUserRequest.Password)} is mandatory");
        }
        private string EnsureCurrentUser()
        {
            var token = TokenParser.Parse(TokenReader.Read());
            var username = token?.Subject;
            if (string.IsNullOrWhiteSpace(token?.Subject))
                throw new ArgumentException("User is not authorized");
            return username;
        }
        private void EnsureBankAccountIsValid(List<IBankAccount> bankAccounts)
        {
            if (bankAccounts == null || bankAccounts.Any() == false)
                throw new InvalidArgumentException($"At least one bank account must to be informed");

            bankAccounts.ForEach(bank =>
            {
                if (string.IsNullOrWhiteSpace(bank.AccountNumber))
                    throw new InvalidArgumentException($"Bank {nameof(bank.AccountNumber)} is mandatory");

                if (bank.AccountType == AccountType.Undefined)
                    throw new InvalidArgumentException($"Bank {nameof(bank.AccountType)} is mandatory");

                if (string.IsNullOrWhiteSpace(bank.RoutingNumber))
                    throw new InvalidArgumentException($"Bank {nameof(bank.RoutingNumber)} is mandatory");

                if (string.IsNullOrWhiteSpace(bank.AccountHolderName))
                    throw new InvalidArgumentException($"Bank {nameof(bank.AccountHolderName)} is mandatory");
            });
        }
        private void EnsureNoDuplicatedBankAccount(List<IBankAccount> bankAccounts)
        {
            var hasDuplicated = bankAccounts
                .GroupBy(b => new { b.AccountNumber, b.RoutingNumber })
                .Any(group => group.Count() > 1);

            if (hasDuplicated)
                throw new InvalidArgumentException("There is duplicated bank accounts in the payload, please verify");
        }
        private static string GenerateUniqueId()
        {
            return Guid.NewGuid().ToString("N");
        }
    }
}