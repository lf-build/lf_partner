﻿using System;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.Partner.Persistence
{
    public class PartnerRepositoryFactory : IPartnerRepositoryFactory
    {
        public PartnerRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IPartnerRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfigurationFactory = Provider.GetService<IMongoConfigurationFactory>();
            var configuration = mongoConfigurationFactory.Get(reader, Provider.GetService<ILogger>());
            return new PartnerRepository(tenantService, configuration);
        }
    }
}
