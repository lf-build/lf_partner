﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Serializers;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Partner.Persistence
{
    public class PartnerRepository : MongoRepository<IPartner, Partner>, IPartnerRepository
    {
        static PartnerRepository()
        {
            BsonClassMap.RegisterClassMap<Partner>(map =>
            {
                map.AutoMap();
                map.MapProperty(p => p.Address).SetIgnoreIfNull(true);
                var type = typeof(Partner);           
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<Address>(map =>
            {
                map.AutoMap();
                var type = typeof(Address);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<Contact>(map =>
            {
                map.AutoMap();
                var type = typeof(Contact);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<PrincipalOwner>(map =>
            {
                map.AutoMap();
                var type = typeof(PrincipalOwner);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<BankAccount>(map =>
            {
                map.AutoMap();
                var type = typeof(BankAccount);
                map.MapProperty(p => p.AccountType).SetSerializer(new EnumSerializer<AccountType>(BsonType.String));
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
            BsonClassMap.RegisterClassMap<TimeBucket>(map =>
            {
                map.AutoMap();
                map.MapMember(m => m.Time).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(TimeBucket);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(false);
            });
        }

        public PartnerRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "partner")
        {

        }

        public async Task<bool> Any(string partnerId)
        {
           
            return await Collection.Find((p => p.TenantId == TenantService.Current.Id && p.PartnerId== partnerId)).FirstOrDefaultAsync() != null;

        }

        public async Task<IEnumerable<IPartner>> GetAll()
        {
            return await Query.Where(p => p.TenantId == TenantService.Current.Id).ToListAsync();
        }

        public async Task<IPartner> GetByPartnerId(string id)
        {           
            return await Collection.Find((p => p.TenantId == TenantService.Current.Id && p.PartnerId == id)).FirstOrDefaultAsync();


        }

        public void Update(string partnerId, IPartner data)
        {
            data.PartnerId = partnerId;
            data.TenantId = TenantService.Current.Id;
            data.Id = null;

            var builder = new FilterDefinitionBuilder<IPartner>();
            var filter = builder.And(new[]
            {
                builder.Eq(f => f.TenantId, TenantService.Current.Id),
                builder.Eq(f => f.PartnerId, partnerId),
            });
            Collection.ReplaceOne(filter, data);
        }

        public async Task UpdateContacts(string partnerId, List<IContact> contacts)
        {
            if (contacts == null) throw new ArgumentNullException(nameof(contacts));
            if (string.IsNullOrWhiteSpace(partnerId))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(partnerId));
            if (contacts.Count == 0)
                throw new ArgumentException("Value cannot be an empty collection.", nameof(contacts));

            var builder = new FilterDefinitionBuilder<IPartner>();
            var filter = builder.And(new[]
            {
                builder.Eq(f => f.TenantId, TenantService.Current.Id),
                builder.Eq(f => f.PartnerId, partnerId),
            });

            await Collection.UpdateOneAsync(filter, new UpdateDefinitionBuilder<IPartner>().Set(m => m.Contacts, contacts));
        }
    }
}
