﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using MongoDB.Bson.Serialization;
using System.Collections.Generic;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace LendFoundry.Partner.Persistence
{
    public class PartnerUserRepository : MongoRepository<IPartnerUser, PartnerUser>, IPartnerUserRepository
    {
        static PartnerUserRepository()
        {
            BsonClassMap.RegisterClassMap<PartnerUser>(map =>
            {
                map.AutoMap();
                var type = typeof(PartnerUser);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });           
        }

        public PartnerUserRepository(ITenantService tenantService, IMongoConfiguration configuration)
            : base(tenantService, configuration, "partner-user")
        {

        }

        public async Task<IPartnerUser> GetByUserId(string userId)
        {
            ObjectId obj;
            if (!ObjectId.TryParse(userId, out obj))
                return null;
            return await Collection.Find((x => x.TenantId == TenantService.Current.Id && x.UserId == userId)).FirstOrDefaultAsync();

        }


        public async Task<IPartnerUser> GetByPartnerandUserId(string partnerId, string userId)
        {
            ObjectId obj;
            if (!ObjectId.TryParse(userId, out obj))
                return null;
            return await Collection.Find((m => m.TenantId == TenantService.Current.Id && m.PartnerId == partnerId && m.UserId == userId)).FirstOrDefaultAsync();

        }

        public async Task<bool> Any(string partnerId, string userId)
        {
            ObjectId obj;          
            if (!ObjectId.TryParse(userId, out obj))
                return false;
            return await Collection.Find((m => m.TenantId == TenantService.Current.Id && m.PartnerId == partnerId && m.UserId == userId)).FirstOrDefaultAsync() != null;
        }

        public async Task<List<IPartnerUser>> GetByPartnerId(string partnerId)
        {            
            return await Collection.Find((x => x.TenantId == TenantService.Current.Id && x.PartnerId == partnerId)).ToListAsync();

        }
    }
}
