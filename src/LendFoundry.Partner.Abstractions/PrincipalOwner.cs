﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;

namespace LendFoundry.Partner
{
    public class PrincipalOwner : IPrincipalOwner
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }

        public string SSN { get; set; }

        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress Address { get; set; }

        public string OwnedPercentage { get; set; }
    }
}
