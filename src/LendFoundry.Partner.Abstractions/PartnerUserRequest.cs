﻿namespace LendFoundry.Partner
{
    public class PartnerUserRequest : IPartnerUserRequest
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
    }
}
