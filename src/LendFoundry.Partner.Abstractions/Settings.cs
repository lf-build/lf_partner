﻿using System;

namespace LendFoundry.Partner
{
    public class Settings
    {
        public static string ServiceName => Environment.GetEnvironmentVariable($"CONFIGURATION_NAME") ?? "partner";
    }
}
