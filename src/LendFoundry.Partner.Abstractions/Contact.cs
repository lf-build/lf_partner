﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Partner
{
    public class Contact : IContact
    {
        public string ContactId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string JobTitle { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneExt { get; set; }
        public bool IsPrimary { get; set; }
        public string Email { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string UserId {get; set;}
      
    }
}
