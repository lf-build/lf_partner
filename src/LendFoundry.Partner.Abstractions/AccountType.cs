﻿namespace LendFoundry.Partner
{
    public enum AccountType
    {
        Undefined,
        Savings,
        Checking,
        Others
    }
}