﻿namespace LendFoundry.Partner
{
    public class ContactUpdateRequest : IContactUpdateRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string JobTitle { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string PhoneExt { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
