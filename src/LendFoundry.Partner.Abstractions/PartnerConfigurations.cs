﻿using LendFoundry.Foundation.Client;
using System.Collections.Generic;

namespace LendFoundry.Partner
{
    public class PartnerConfigurations : IPartnerConfigurations, IDependencyConfiguration
    {
        public string InitialEnrollmentStatusCode { get; set; }

        public string StatusWorkFlow { get; set; }

        public string[] DefaultRoles { get; set; }

        public EventMapping[] Events { get; set; }

        public Dictionary<string, string> Dependencies { get; set; }

        public string Database { get; set; }

        public string ConnectionString { get; set; }
    }
}
