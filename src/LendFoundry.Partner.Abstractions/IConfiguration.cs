﻿

namespace LendFoundry.Partner
{
    public interface IConfiguration
    {
        EventMapping[] Events { get; set; }
    }
}
