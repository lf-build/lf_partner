﻿
namespace LendFoundry.Partner
{
    public enum Status
    {
        requested = 0,
        activate = 1,
        deactivate = 2       
    }
}
