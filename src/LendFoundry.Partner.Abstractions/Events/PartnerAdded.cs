﻿namespace LendFoundry.Partner.Events
{
    public class PartnerAdded
    {
        public PartnerAdded(IPartner partner)
        {
            Partner = partner;
        }

        public IPartner Partner { get; set; }
    }
}
