﻿namespace LendFoundry.Partner.Events
{
    public class PartnerUpdated
    {
        public PartnerUpdated(IPartner partner)
        {
            Partner = partner;
        }

        public IPartner Partner { get; set; }
    }
}
