﻿namespace LendFoundry.Partner.Events
{
    public class PartnerRemoved
    {
        public PartnerRemoved(IPartner partner)
        {
            Partner = partner;
        }

        public IPartner Partner { get; set; }
    }
}
