﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Partner
{
    public class LoginRequestModel : ILoginRequestModel
    {
        public string Password { get; set; }
        public string Portal { get; set; }
        public string Username { get; set; }
    }
}
