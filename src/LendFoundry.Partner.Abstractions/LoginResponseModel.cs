﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Partner
{
    public class LoginResponseModel
    {
        public string UserId { get; set; }
        public string PartnerId { get; set; }

        public string Token { get; set; }
    }
}
