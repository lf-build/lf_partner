﻿namespace LendFoundry.Partner
{
    public enum PartnerType
    {
        BROKER,
        LEADAGG,
        AFFILIATE,
        MERCHANT,
        LENDER,
        INVESTOR
    }
}
