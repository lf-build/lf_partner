﻿using LendFoundry.Foundation.Persistence;

namespace LendFoundry.Partner
{
    public class PartnerUser : Aggregate, IPartnerUser
    {
        public string UserId { get; set; }
        public string PartnerId { get; set; }
    }
}
