﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Partner
{
    public interface IContactUserRequest
    {
        string FirstName { get; set; }
        string LastName { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
        string Email { get; set; }
        bool IsPrimary { get; set; }

        List<string> Roles { get; set; }
    }
}
