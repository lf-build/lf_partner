﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Partner
{
    public interface ILoginRequestModel
    {
         string Password { get; set; }
         string Portal { get; set; }
         string Username { get; set; }
    }
}
