﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Partner
{
    public interface IPartnerUserRepository : IRepository<IPartnerUser>
    {
        Task<IPartnerUser> GetByUserId(string userId);

        Task<IPartnerUser> GetByPartnerandUserId(string partnerId, string userId);

        Task<bool> Any(string partnerId, string userId);

        Task<List<IPartnerUser>> GetByPartnerId(string partnerId);
    }
}
