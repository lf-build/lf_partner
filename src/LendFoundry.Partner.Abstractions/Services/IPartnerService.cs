﻿using LendFoundry.Security.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Partner
{
    public interface IPartnerService
    {
        Task<IPartner> Add(IPartner partner);
        Task Remove(string partnerId);
      
        Task Update(string partnerId, IUpdatePartner partner);
        Task<IEnumerable<IPartner>> GetAll();
        Task<IPartner> Get(string partnerId);
        Task UpdateActiveContact(string partnerId, IContactUpdateRequest contactUpdateRequest);
        Task DeletePartnerUser(string partnerId, string userId);
        Task<IPartnerUser> AddPartnerUser(string partnerId, IPartnerUserRequest partnerUserRequest);
        Task AssosiateUserToPartner(string partnerId, string userId);
        Task<IPartner> AddContacts(string partnerId, IContact contact);
        Task UpdateContacts(string partnerId, string contactId, IContact contact);
        Task DeleteContact(string partnerId, string contactId);
        Task AttachUser(string partnerId, string contactId, IPartnerUserRequest partnerUserRequest);
        Task DetachUser(string partnerId, string contactId, string userId);

        Task<IBankAccount> AddBank(string partnerId, IBankAccount bankAccount);

        Task<IBankAccount> UpdateBank(string partnerId, string bankId, IBankAccount bankAccount);

        Task DeleteBank(string partnerId, string bankId);

        Task<IEnumerable<IBankAccount>> GetAllBanks(string partnerId);

        Task<IBankAccount> GetBank(string partnerId, string bankId);

        Task SetBankAsPrimary(string partnerId, string bankId);

        Task ChangeStatus(string partnerId, string statusCode);

        Task SetContactAsPrimary(string partnerId, string contactId);

        Task<IPartner> AddUser(string partnerId, IContactUserRequest contactUserRequest);

        Task<List<IContact>> GetContacts(string partnerId);

        Task<List<IUserInfo>> GetPartnerUsers(string partnerId);

        Task<IPartnerUser> GetByUserId(string userId);

        Task<LoginResponseModel> Login(ILoginRequestModel login);
    }
}
