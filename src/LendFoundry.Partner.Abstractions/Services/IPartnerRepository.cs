﻿using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Partner
{
    public interface IPartnerRepository : IRepository<IPartner>
    {
        Task<IEnumerable<IPartner>> GetAll();
        Task<bool> Any(string partnerId);
        Task<IPartner> GetByPartnerId(string id);
        void Update(string partnerId, IPartner partner);
         Task UpdateContacts(string partnerId, List<IContact> contacts);
    }
}
