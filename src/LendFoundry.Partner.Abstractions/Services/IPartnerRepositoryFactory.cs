﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Partner
{
    public interface IPartnerRepositoryFactory
    {
        IPartnerRepository Create(ITokenReader reader);
    }
}
