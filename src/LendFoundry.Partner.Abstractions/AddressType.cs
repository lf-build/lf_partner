﻿
namespace LendFoundry.Partner
{
    public enum AddressType
    {
        Residence = 1,
        Work = 2,
        Permanant = 3,
        Current = 4,
        Bank = 5
    }
}
