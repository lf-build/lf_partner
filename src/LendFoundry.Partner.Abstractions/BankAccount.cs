﻿namespace LendFoundry.Partner
{
    public class BankAccount : IBankAccount
    {
        public string Id { get; set; }

        public string AccountNumber { get; set; }

        public string RoutingNumber { get; set; }

        public AccountType AccountType { get; set; }

        public bool IsPrimary { get; set; }

        public string AccountHolderName { get; set; }

        public string BranchAddress { get; set; }

        public string Name { get; set; }

    }
}
