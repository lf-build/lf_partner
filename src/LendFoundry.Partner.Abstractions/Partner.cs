﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LendFoundry.Partner
{
    public class Partner : Aggregate, IPartner
    {
        public string PartnerId { get; set; }
        public string Name { get; set; }

        public string DBA { get; set; }
        public string NickName { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        public IAddress Address { get; set; }
        public PartnerType PartnerType { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPrincipalOwner, PrincipalOwner>))]
        public IPrincipalOwner PrincipalOwner { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IContact, Contact>))]
        public List<IContact> Contacts { get; set; }

        public TimeBucket StatusChangeDate { get; set; }

        public string StatusChangedBy { get; set; }
        public string Notes { get; set; }

        public string Status { get; set; }  

        public string EIN { get; set; }

        public string WebSite { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBankAccount, BankAccount>))]
        public List<IBankAccount> BankAccounts { get; set; }

        public TimeBucket InCorporationDate { get; set; }
        public TimeBucket SubmittedDate { get; set; }
        public TimeBucket ActivatedOn { get; set; }

    }
}
