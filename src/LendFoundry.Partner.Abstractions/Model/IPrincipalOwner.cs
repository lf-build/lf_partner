﻿using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;

namespace LendFoundry.Partner
{
    public interface IPrincipalOwner
    {
        string FirstName { get; set; }
        string LastName { get; set; }
        DateTime DateOfBirth { get; set; }

        string SSN { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        IAddress Address { get; set; }

        string OwnedPercentage { get; set; }
    }
}
