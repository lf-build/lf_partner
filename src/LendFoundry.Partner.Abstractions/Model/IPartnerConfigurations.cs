﻿using LendFoundry.Foundation.Client;

namespace LendFoundry.Partner
{
    public interface IPartnerConfigurations : IDependencyConfiguration
    {
        string InitialEnrollmentStatusCode { get; set; }

        string StatusWorkFlow { get; set; }

        string[] DefaultRoles { get; set; }

        EventMapping[] Events { get; set; }

        string ConnectionString { get; set; }
    }
}
