﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Client;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace LendFoundry.Partner
{
    public interface IUpdatePartner : IAggregate
    {
        string PartnerId { get; set; }
        string Name { get; set; }
        string DBA { get; set; }
        string NickName { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IAddress, Address>))]
        IAddress Address { get; set; }
        PartnerType PartnerType { get; set; }
        [JsonConverter(typeof(InterfaceConverter<IPrincipalOwner, PrincipalOwner>))]
        IPrincipalOwner PrincipalOwner { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IContact, Contact>))]
        List<IContact> Contacts { get; set; }
      

        string StatusChangedBy { get; set; }

        string Notes { get; set; }

        string Status { get; set; }

        string EIN { get; set; }

        string WebSite { get; set; }

        [JsonConverter(typeof(InterfaceListConverter<IBankAccount, BankAccount>))]
        List<IBankAccount> BankAccounts { get; set; }

      
        DateTimeOffset InCorporationDate { get; set; }

    }
}
