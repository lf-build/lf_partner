﻿
namespace LendFoundry.Partner
{
   public interface IBankAccount
    {
        string Id { get; set; }
        string AccountNumber { get; set; }
        string RoutingNumber { get; set; }
        AccountType AccountType { get; set; }
        bool IsPrimary { get; set; }
        string AccountHolderName { get; set; }

        string BranchAddress { get; set; }

        string Name { get; set; }
    }
}
