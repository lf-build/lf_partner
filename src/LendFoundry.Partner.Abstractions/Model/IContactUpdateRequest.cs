﻿namespace LendFoundry.Partner
{
    public interface IContactUpdateRequest
    {
        string FirstName { get; set; }
        string LastName { get; set; }
        string JobTitle { get; set; }
        string Email { get; set; }
        string PhoneNumber { get; set; }
        string PhoneExt { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
      
    }
}
