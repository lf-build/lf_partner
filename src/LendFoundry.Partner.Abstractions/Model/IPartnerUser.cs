﻿using LendFoundry.Foundation.Persistence;

namespace LendFoundry.Partner
{
    public interface IPartnerUser : IAggregate
    {
        string UserId { get; set; }
        string PartnerId { get; set; }
    }
}
