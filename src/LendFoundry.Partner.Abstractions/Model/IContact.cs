﻿using System;

namespace LendFoundry.Partner
{
    public interface IContact
    { 
        string ContactId { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string JobTitle { get; set; }
        string PhoneNumber { get; set; }
        string PhoneExt { get; set; }
        bool IsPrimary { get; set; }
        string Email { get; set; }
        DateTime DateOfBirth { get; set; }
        string UserId { get; set; }
        
    }
}
