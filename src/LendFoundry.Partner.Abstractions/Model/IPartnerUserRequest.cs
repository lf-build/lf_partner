﻿namespace LendFoundry.Partner
{
    public interface IPartnerUserRequest
    {
        string UserName { get; set; }
        string Password { get; set; }
        string Email { get; set; }
    }
}
