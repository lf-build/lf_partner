﻿using LendFoundry.Foundation.Client;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Security.Identity;

namespace LendFoundry.Partner.Client
{
    public class PartnerService : IPartnerService
    {
        public PartnerService(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public async Task<IPartner> Add(IPartner partner)
        {
            var request = new RestRequest("/", Method.POST);
            request.AddBody(partner);
            return await Client.ExecuteAsync<Partner>(request);
        }
        public async Task<IPartner> AddContacts(string partnerId, IContact contact)
        {
            var request = new RestRequest("/{partnerId}/contact", Method.POST);
            request.AddUrlSegment("partnerId", partnerId);
            request.AddJsonBody(contact);
            return await Client.ExecuteAsync<Partner>(request);
        }
        public async Task UpdateContacts(string partnerId, string contactId, IContact contact)
        {
            var request = new RestRequest("/{partnerId}/contact/{contactId}", Method.PUT);
            request.AddUrlSegment("partnerId", partnerId);
            request.AddUrlSegment("contactId", contactId);
            request.AddJsonBody(contact);
            await Client.ExecuteAsync(request);
        }

        public async Task DeleteContact(string partnerId, string contactId)
        {
            var request = new RestRequest("/{partnerId}/contact/{contactId}", Method.DELETE);
            request.AddUrlSegment("partnerId", partnerId);
            request.AddUrlSegment("contactId", contactId);
            await Client.ExecuteAsync(request);
        }

        public async Task AttachUser(string partnerId, string contactId, IPartnerUserRequest partnerUserRequest)
        {
            var request = new RestRequest("/{partnerId}/attach/{contactId}", Method.PUT);
            request.AddUrlSegment("partnerId", partnerId);
            request.AddUrlSegment("contactId", contactId);
            request.AddJsonBody(partnerUserRequest);
            await Client.ExecuteAsync(request);
        }
        public async Task DetachUser(string partnerId, string contactId, string userId)
        {
            var request = new RestRequest("/{partnerId}/Detach/{contactId}/{userId}", Method.PUT);
            request.AddUrlSegment("partnerId", partnerId);
            request.AddUrlSegment("contactId", contactId);
            request.AddUrlSegment("userId", userId);
            await Client.ExecuteAsync(request);
        }
        public async Task<IEnumerable<IPartner>> GetAll()
        {
            var request = new RestRequest("/all", Method.GET);
            return await Client.ExecuteAsync<List<IPartner>>(request);
        }

        public async Task<IPartner> Get(string partnerId)
        {
            var request = new RestRequest("/{id}", Method.GET);
            request.AddUrlSegment("id", partnerId);
            return await Client.ExecuteAsync<Partner>(request);
        }

        public async Task Update(string partnerId, IUpdatePartner data)
        {
            var request = new RestRequest("/{id}", Method.PUT);
            request.AddUrlSegment("id", partnerId);
            request.AddBody(data);
            await Client.ExecuteAsync(request);
        }
      
        public Task Remove(string partnerId)
        {
            throw  new Exception("DevOps use only"); 
        }

        public async Task<IPartnerUser> AddPartnerUser(string partnerId, IPartnerUserRequest partnerUserRequest)
        {
            var request = new RestRequest("/{partnerId}/users", Method.POST);
            request.AddUrlSegment("partnerId", partnerId);
            request.AddJsonBody(partnerUserRequest);
            return await Client.ExecuteAsync<PartnerUser>(request);
        }

        public async Task AssosiateUserToPartner(string partnerId, string userId)
        {
            var request = new RestRequest("/{partnerId}/users/{userId}", Method.POST);
            request.AddUrlSegment("partnerId", partnerId);
            request.AddUrlSegment("userId", userId);

            await Client.ExecuteAsync(request);
        }

        public async Task DeletePartnerUser(string partnerId, string userId)
        {
            var request = new RestRequest("/{partnerId}/users/{userId}", Method.DELETE);
            request.AddUrlSegment("partnerId", partnerId);
            request.AddUrlSegment("userId", userId);
            await Client.ExecuteAsync(request);
        }

        public async Task UpdateActiveContact(string partnerId, IContactUpdateRequest contactUpdateRequest)
        {
            var request = new RestRequest("/{partnerId}/active-contact", Method.PUT);
            request.AddUrlSegment("partnerId", partnerId);
            request.AddJsonBody(contactUpdateRequest);
            await Client.ExecuteAsync(request);
        }


        public async Task<IBankAccount> AddBank(string partnerId, IBankAccount bankAccount)
        {
            var request = new RestRequest("/{partnerId}/banks", Method.POST);
            request.AddUrlSegment("partnerId", partnerId);
            request.AddJsonBody(bankAccount);
            return await Client.ExecuteAsync<BankAccount>(request);
        }

        public async Task<IBankAccount> UpdateBank(string partnerId, string bankId, IBankAccount bankAccount)
        {
            var request = new RestRequest("/{partnerId}/banks/{bankId}", Method.PUT);
            request.AddUrlSegment("partnerId", partnerId);
            request.AddUrlSegment("bankId", bankId);
            request.AddJsonBody(bankAccount);
            return await Client.ExecuteAsync<BankAccount>(request);
        }

        public async Task DeleteBank(string partnerId, string bankId)
        {
            var request = new RestRequest("/{partnerId}/banks/{bankId}", Method.DELETE);
            request.AddUrlSegment("partnerId", partnerId);
            request.AddUrlSegment("bankId", bankId);
            await Client.ExecuteAsync(request);
        }

        public async Task<IEnumerable<IBankAccount>> GetAllBanks(string partnerId)
        {
            var request = new RestRequest("/{partnerId}/banks", Method.GET);
            request.AddUrlSegment("partnerId", partnerId);           
            return await Client.ExecuteAsync<List<BankAccount>>(request);
        }

        public async Task<IBankAccount> GetBank(string partnerId, string bankId)
        {
            var request = new RestRequest("/{partnerId}/banks/{bankId}", Method.GET);
            request.AddUrlSegment("partnerId", partnerId);
            request.AddUrlSegment("bankId", bankId);
            return await Client.ExecuteAsync<BankAccount>(request);
        }

        public async Task SetBankAsPrimary(string partnerId, string bankId)
        {
            var request = new RestRequest("/{partnerId}/banks/{bankId}/primary", Method.PUT);
            request.AddUrlSegment("partnerId", partnerId);
            request.AddUrlSegment("bankId", bankId);
            await Client.ExecuteAsync(request);

        }
        public async Task ChangeStatus(string partnerId, string statusCode)
        {
            var request = new RestRequest("/{partnerId}/status/{statusCode}", Method.PUT);
            request.AddUrlSegment("partnerId", partnerId);
            request.AddUrlSegment("statusCode", statusCode);           
            await Client.ExecuteAsync(request);

        }

        public async Task SetContactAsPrimary(string partnerId, string contactId)
        {
            var request = new RestRequest("/{partnerId}/contacts/{contactId}/primary", Method.PUT);
            request.AddUrlSegment("partnerId", partnerId);
            request.AddUrlSegment("contactId", contactId);
            await Client.ExecuteAsync(request);
        }

        public async Task<IPartner> AddUser(string partnerId, IContactUserRequest contactUserRequest)
        {
            var request = new RestRequest("/{partnerId}/adduser", Method.POST);
            request.AddUrlSegment("partnerId", partnerId);
            request.AddJsonBody(contactUserRequest);
            return await Client.ExecuteAsync<Partner>(request);
        }

        public async Task<List<IContact>> GetContacts(string partnerId)
        {
            var request = new RestRequest("/{partnerId}/getcontacts", Method.GET);
            request.AddUrlSegment("partnerId", partnerId);
            var result = await Client.ExecuteAsync<List<Contact>>(request);
            return new List<IContact>(result);
          
        }

        public async Task<List<IUserInfo>> GetPartnerUsers(string partnerId)
        {
            var request = new RestRequest("/{partnerId}/partnerusers", Method.GET);
            request.AddUrlSegment("partnerId", partnerId);
            var result = await Client.ExecuteAsync<List<UserInfo>>(request);
            return new List<IUserInfo>(result);
        }

        public async Task<IPartnerUser> GetByUserId(string userId)
        {
            var request = new RestRequest("/{userId}/userid", Method.GET);
            request.AddUrlSegment("userId", userId);          
            return await Client.ExecuteAsync<PartnerUser>(request);
        }

        public Task<LoginResponseModel> Login(ILoginRequestModel login)
        {
            throw new NotImplementedException();
        }
    }
}


