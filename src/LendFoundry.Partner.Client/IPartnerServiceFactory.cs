﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.Partner.Client
{
    public interface IPartnerServiceFactory
    {
        IPartnerService Create(ITokenReader reader);
    }
}
