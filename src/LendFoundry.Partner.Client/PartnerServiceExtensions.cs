﻿using LendFoundry.Security.Tokens;
using System;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif

namespace LendFoundry.Partner.Client
{
    public static class PartnerServiceExtensions
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public static IServiceCollection AddPartner(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IPartnerServiceFactory>(p => new PartnerServiceFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IPartnerServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddPartner(this IServiceCollection services, Uri uri)
        {
            services.AddTransient<IPartnerServiceFactory>(p => new PartnerServiceFactory(p, uri));
            services.AddTransient(p => p.GetService<IPartnerServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }

        public static IServiceCollection AddPartner(this IServiceCollection services)
        {
            services.AddTransient<IPartnerServiceFactory>(p => new PartnerServiceFactory(p));
            services.AddTransient(p => p.GetService<IPartnerServiceFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}
