﻿using LendFoundry.Foundation.Client;
using LendFoundry.Security.Tokens;
using System;
using LendFoundry.Foundation.Logging;
#if DOTNET2
using Microsoft.Extensions.DependencyInjection;
#else
using Microsoft.Framework.DependencyInjection;
#endif
namespace LendFoundry.Partner.Client
{
    public class PartnerServiceFactory : IPartnerServiceFactory
    {
        [Obsolete("Need to use the overloaded with Uri")]
        public PartnerServiceFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Uri = new UriBuilder("http", endpoint, port).Uri;
        }

        public PartnerServiceFactory(IServiceProvider provider, Uri uri = null)
        {
            Provider = provider;
            Uri = uri;
        }

        private IServiceProvider Provider { get; set; }
        private Uri Uri { get; }

        public IPartnerService Create(ITokenReader reader)
        {
            var uri = Uri;
            if (uri == null)
            {
                var logger = Provider.GetService<ILoggerFactory>().Create(NullLogContext.Instance);
                uri = Provider.GetRequiredService<IDependencyServiceUriResolverFactory>().Create(reader, logger).Get("partner");
            }
            var client = Provider.GetServiceClient(reader, uri);
            return new PartnerService(client);
        }
    }
}
