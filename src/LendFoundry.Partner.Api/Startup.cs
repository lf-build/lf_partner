﻿using LendFoundry.Partner.Persistence;
using LendFoundry.StatusManagement.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.NumberGenerator.Client;
using LendFoundry.Security.Identity.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using LendFoundry.EventHub;
using LendFoundry.Foundation.ServiceDependencyResolver;
using LendFoundry.Configuration;
using System.Collections.Generic;

#if DOTNET2
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.AspNetCore.Http;
using System.IO;
#else
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Documentation;
#endif

namespace LendFoundry.Partner.Api
{
    internal class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            // Register the Swagger generator, defining one or more Swagger documents
#if DOTNET2
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("docs", new Info
                {
                    Version = PlatformServices.Default.Application.ApplicationVersion,
                    Title = "Partner"
                });
                c.AddSecurityDefinition("Bearer", new ApiKeyScheme() 
                {
                    Type = "apiKey",
                    Name = "Authorization",
                    Description = "For accessing the API a valid JWT token must be passed in all the queries in the 'Authorization' header. The syntax used in the 'Authorization' header should be Bearer xxxxx.yyyyyy.zzzz",
                    In = "header"
                });
                c.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>> 
                { 
					{ "Bearer", new string[]{} }
                });
                c.DescribeAllEnumsAsStrings();
                c.IgnoreObsoleteProperties();
                c.DescribeStringEnumsInCamelCase();
                c.IgnoreObsoleteActions();
                var basePath = PlatformServices.Default.Application.ApplicationBasePath;
                var xmlPath = Path.Combine(basePath, "LendFoundry.Partner.Api.xml");
                c.IncludeXmlComments(xmlPath);
            });
            services.TryAddSingleton<IHttpContextAccessor, HttpContextAccessor>();

#else
            services.AddSwaggerDocumentation();
#endif
            // services
            services.AddTenantTime();
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantService();
            services.AddEventHub(Settings.ServiceName);
            services.AddConfigurationService<PartnerConfigurations>(Settings.ServiceName);
            services.AddDependencyServiceUriResolver<PartnerConfigurations>(Settings.ServiceName);
            services.AddIdentityService();
            services.AddNumberGeneratorService();
            services.AddStatusManagementService();
            services.AddMongoConfiguration(Settings.ServiceName);

            // interface resolvers
            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            });
            services.AddTransient(provider => provider.GetRequiredService<IConfigurationService<PartnerConfigurations>>().Get());           
            services.AddTransient<IPartnerService, PartnerService>();
            services.AddTransient<IPartnerRepository, PartnerRepository>();
            services.AddTransient<IPartnerUserRepository, PartnerUserRepository>();
            services.AddTransient<IPartnerRepositoryFactory, PartnerRepositoryFactory>();
            services.AddTransient<IPartnerConfigurations, PartnerConfigurations>();

            services.AddMvc().AddLendFoundryJsonOptions((options) =>
            {
                options.AddInterfaceConverter<IAddress, Address>();
            });
            services.AddCors();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseHealthCheck();
		app.UseCors(env);

            // Enable middleware to serve generated Swagger as a JSON endpoint.
#if DOTNET2
            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/docs/swagger.json", "Partner Service");
            });
#else
            app.UseSwaggerDocumentation();
#endif
            
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseMvc();
            app.UseConfigurationCacheDependency();
        }
    }
}
