﻿using LendFoundry.Foundation.Services;
using LendFoundry.Security.Identity;
#if DOTNET2
using Microsoft.AspNetCore.Mvc;
#else
using Microsoft.AspNet.Mvc;
#endif
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Partner.Api.Controllers
{
    /// <summary>
    /// ApiController class
    /// </summary>
    /// <seealso cref="ExtendedController" />
    [Route("/")]
    public class ApiController : ExtendedController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ApiController"/> class.
        /// </summary>
        /// <param name="service">The service.</param>
        public ApiController(IPartnerService service)
        {
            Service = service;
        }
        private static NoContentResult NoContentResult { get; } = new NoContentResult();

        private IPartnerService Service { get; }
        
        /// <summary>
        /// Adds the specified partner.
        /// </summary>
        /// <param name="partner">The partner.</param>
        /// <returns></returns>
        [HttpPost("/")]
        [ProducesResponseType(typeof(IPartner), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> Add([FromBody]Partner partner)
        {
            return await ExecuteAsync(async () => Ok(await Service.Add(partner)));
        }

        /// <summary>
        /// Adds the contact.
        /// </summary>
        /// <param name="partnerId">The partner identifier.</param>
        /// <param name="contact">The contact.</param>
        /// <returns></returns>
        [HttpPost("/{partnerId}/contact")]
        [ProducesResponseType(typeof(IPartner), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddContact(string partnerId, [FromBody]Contact contact)
        {
            return await ExecuteAsync(async () => Ok(await Service.AddContacts(partnerId, contact)));
        }

        /// <summary>
        /// Adds the bank.
        /// </summary>
        /// <param name="partnerId">The partner identifier.</param>
        /// <param name="bankAccount">The bank account.</param>
        /// <returns></returns>
        [HttpPost("/{partnerId}/banks")]
        [ProducesResponseType(typeof(IBankAccount), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddBank(string partnerId, [FromBody]BankAccount bankAccount)
        {
            return await ExecuteAsync(async () => Ok(await Service.AddBank(partnerId, bankAccount)));

        }

        /// <summary>
        /// Updates the bank.
        /// </summary>
        /// <param name="partnerId">The partner identifier.</param>
        /// <param name="bankId">The bank identifier.</param>
        /// <param name="bankAccount">The bank account.</param>
        /// <returns></returns>
        [HttpPut("/{partnerId}/banks/{bankId}")]
        [ProducesResponseType(typeof(IBankAccount), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> UpdateBank(string partnerId, string bankId, [FromBody]BankAccount bankAccount)
        {
            return await ExecuteAsync(async () => Ok(await Service.UpdateBank(partnerId, bankId, bankAccount)));

        }

        /// <summary>
        /// Deletes the bank.
        /// </summary>
        /// <param name="partnerId">The partner identifier.</param>
        /// <param name="bankId">The bank identifier.</param>
        /// <returns></returns>
        [HttpDelete("/{partnerId}/banks/{bankId}")]
        [ProducesResponseType(204)]
        public async Task<IActionResult> DeleteBank(string partnerId, string bankId)
        {
            return await ExecuteAsync(async () => { await Service.DeleteBank(partnerId, bankId); return NoContentResult; });
        }

        /// <summary>
        /// Gets all banks.
        /// </summary>
        /// <param name="partnerId">The partner identifier.</param>
        /// <returns></returns>
        [HttpGet("/{partnerId}/banks")]
        [ProducesResponseType(typeof(IEnumerable<IBankAccount>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetAllBanks(string partnerId)
        {
            return await ExecuteAsync(async () => Ok(await Service.GetAllBanks(partnerId)));

        }

        /// <summary>
        /// Gets the bank.
        /// </summary>
        /// <param name="partnerId">The partner identifier.</param>
        /// <param name="bankId">The bank identifier.</param>
        /// <returns></returns>
        [HttpGet("/{partnerId}/banks/{bankId}")]
        [ProducesResponseType(typeof(IBankAccount), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetBank(string partnerId, string bankId)
        {
            return await ExecuteAsync(async () => Ok(await Service.GetBank(partnerId, bankId)));

        }

        /// <summary>
        /// Sets the bank asprimary.
        /// </summary>
        /// <param name="partnerId">The partner identifier.</param>
        /// <param name="bankId">The bank identifier.</param>
        /// <returns></returns>
        [HttpPut("/{partnerId}/banks/{bankId}/primary")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> SetBankAsprimary(string partnerId, string bankId)
        {
            return await ExecuteAsync(async () => { await Service.SetBankAsPrimary(partnerId, bankId); return NoContentResult; });

        }

        /// <summary>
        /// Sets the contact asprimary.
        /// </summary>
        /// <param name="partnerId">The partner identifier.</param>
        /// <param name="contactId">The contact identifier.</param>
        /// <returns></returns>
        [HttpPut("/{partnerId}/contacts/{contactId}/primary")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> SetContactAsprimary(string partnerId, string contactId)
        {
            return await ExecuteAsync(async () => { await Service.SetContactAsPrimary(partnerId, contactId); return NoContentResult; });

        }

        /// <summary>
        /// Updates the contact.
        /// </summary>
        /// <param name="partnerId">The partner identifier.</param>
        /// <param name="contactId">The contact identifier.</param>
        /// <param name="contact">The contact.</param>
        /// <returns></returns>
        [HttpPut("/{partnerId}/contact/{contactId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> UpdateContact(string partnerId, string contactId, [FromBody]Contact contact)
        {
            return await ExecuteAsync(async () =>
            {
                await Service.UpdateContacts(partnerId, contactId, contact);
                return NoContentResult;
            });

        }

        /// <summary>
        /// Deletes the contact.
        /// </summary>
        /// <param name="partnerId">The partner identifier.</param>
        /// <param name="contactId">The contact identifier.</param>
        /// <returns></returns>
        [HttpDelete("/{partnerId}/contact/{contactId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> DeleteContact(string partnerId, string contactId)
        {
            return await ExecuteAsync(async () => { await Service.DeleteContact(partnerId, contactId); return NoContentResult; });
        }
        
        /// <summary>
        /// Attaches the user.
        /// </summary>
        /// <param name="partnerId">The partner identifier.</param>
        /// <param name="contactId">The contact identifier.</param>
        /// <param name="partnerUserRequest">The partner user request.</param>
        /// <returns></returns>
        [HttpPut("/{partnerId}/attach/{contactId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> AttachUser(string partnerId, string contactId, [FromBody]PartnerUserRequest partnerUserRequest)
        {
            return await ExecuteAsync(async () => { await Service.AttachUser(partnerId, contactId, partnerUserRequest); return NoContentResult; });
        }

        /// <summary>
        /// Detaches the user.
        /// </summary>
        /// <param name="partnerId">The partner identifier.</param>
        /// <param name="contactId">The contact identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        [HttpPut("/{partnerId}/detach/{contactId}/{userId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> DetachUser(string partnerId, string contactId, string userId)
        {
            return await ExecuteAsync(async () => { await Service.DetachUser(partnerId, contactId, userId); return NoContentResult; });
        }

        /// <summary>
        /// Gets all.
        /// </summary>
        /// <returns></returns>
        [HttpGet("/all")]
        [ProducesResponseType(typeof(IEnumerable<IPartner>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetAll()
        {
            return await ExecuteAsync(async () => Ok(await Service.GetAll()));

        }

        /// <summary>
        /// Gets the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet("/{id}")]
        [ProducesResponseType(typeof(IPartner), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> Get(string id) =>
         await ExecuteAsync(async () => Ok(await Service.Get(id)));

        /// <summary>
        /// Updates the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="partner">The partner.</param>
        /// <returns></returns>
        [HttpPut("/{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> Update(string id, [FromBody]UpdatePartner partner)
        {
            return await ExecuteAsync(async () =>
            {
                await Service.Update(id, partner);
                return NoContentResult;
            });

        }

        /// <summary>
        /// Deletes the specified identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpDelete("/{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> Delete(string id)
        {
            return await ExecuteAsync(async () =>
            {
                await Service.Remove(id);
                return NoContentResult;
            });

        }

        /// <summary>
        /// Updates the active contact.
        /// </summary>
        /// <param name="partnerId">The partner identifier.</param>
        /// <param name="contactUpdateRequest">The contact update request.</param>
        /// <returns></returns>
        [HttpPut("/{partnerId}/active-contact")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> UpdateActiveContact(string partnerId, [FromBody]ContactUpdateRequest contactUpdateRequest)
        {
            return await ExecuteAsync(async () =>
            {
                await Service.UpdateActiveContact(partnerId, contactUpdateRequest);
                return NoContentResult;
            });
        }

        /// <summary>
        /// Adds the partner user.
        /// </summary>
        /// <param name="partnerId">The partner identifier.</param>
        /// <param name="partnerUser">The partner user.</param>
        /// <returns></returns>
        [HttpPost("/{partnerId}/users")]
        [ProducesResponseType(typeof(IPartnerUser), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> AddPartnerUser(string partnerId, [FromBody]PartnerUserRequest partnerUser)
        {
            return await ExecuteAsync(async () => Ok(await Service.AddPartnerUser(partnerId, partnerUser)));
        }

        /// <summary>
        /// Assosiates the user.
        /// </summary>
        /// <param name="partnerId">The partner identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        [HttpPut("/{partnerId}/users/{userId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> AssosiateUser(string partnerId, string userId)
        {
            return await ExecuteAsync(async () =>
            {
                await Service.AssosiateUserToPartner(partnerId, userId);
                return NoContentResult;
            });

        }

        /// <summary>
        /// Deletes the partner user.
        /// </summary>
        /// <param name="partnerId">The partner identifier.</param>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        [HttpDelete("/{partnerId}/users/{userId}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> DeletePartnerUser(string partnerId, string userId)
        {
            return await ExecuteAsync(async () =>
            {
                await Service.DeletePartnerUser(partnerId, userId);
                return NoContentResult;
            });

        }

        /// <summary>
        /// Changes the status.
        /// </summary>
        /// <param name="partnerId">The partner identifier.</param>
        /// <param name="statusCode">The status code.</param>
        /// <returns></returns>
        [HttpPut("/{partnerId}/status/{statusCode}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> ChangeStatus(string partnerId, string statusCode)
        {
            return await ExecuteAsync(async () => { await Service.ChangeStatus(partnerId, statusCode); return NoContentResult; });
        }

        /// <summary>
        /// Adds the user.
        /// </summary>
        /// <param name="partnerId">The partner identifier.</param>
        /// <param name="partnerContact">The partner contact.</param>
        /// <returns></returns>
        [HttpPost("/{partnerId}/adduser")]
        [ProducesResponseType(typeof(IPartner), 200)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> AddUser(string partnerId, [FromBody]ContactUserRequest partnerContact)
        {
            return await ExecuteAsync(async () => { await Service.AddUser(partnerId, partnerContact); return NoContentResult; });
        }

        /// <summary>
        /// Gets the contact users.
        /// </summary>
        /// <param name="partnerId">The partner identifier.</param>
        /// <returns></returns>
        [HttpGet("/{partnerId}/getcontacts")]
        [ProducesResponseType(typeof(List<IContact>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetContactUsers(string partnerId)
        {
            return await ExecuteAsync(async () => Ok(await Service.GetContacts(partnerId)));

        }

        /// <summary>
        /// Gets the partner users.
        /// </summary>
        /// <param name="partnerId">The partner identifier.</param>
        /// <returns></returns>
        [HttpGet("/{partnerId}/partnerusers")]
        [ProducesResponseType(typeof(List<IUserInfo>), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetPartnerUsers(string partnerId)
        {
            return await ExecuteAsync(async () => Ok(await Service.GetPartnerUsers(partnerId)));

        }

        /// <summary>
        /// Gets the by user identifier.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        [HttpGet("/{userId}/userid")]
        [ProducesResponseType(typeof(IPartnerUser), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        public async Task<IActionResult> GetByUserId(string userId)
        {
            return await ExecuteAsync(async () => Ok(await Service.GetByUserId(userId)));

        }

        /// <summary>
        /// Logins the specified login.
        /// </summary>
        /// <param name="login">The login.</param>
        /// <returns></returns>
        /// <exception cref="InvalidUserOrPasswordException"></exception>
        [HttpPost("/login")]
        [ProducesResponseType(typeof(LoginResponseModel), 200)]
        [ProducesResponseType(typeof(ErrorResult), 404)]
        [ProducesResponseType(typeof(ErrorResult), 400)]
        public async Task<IActionResult> Login([FromBody] LoginRequestModel login)
        {
            try
            {
                if (login == null)
                    throw new InvalidUserOrPasswordException(string.Empty);

                return Ok(await Service.Login(login));
            }
            catch (ArgumentException exception)
            {
                Logger.Warn("Invalid {ParamName}", new { exception.ParamName });
                return new ErrorResult(400, exception.Message);
            }
            catch (InvalidUserOrPasswordException exception)
            {
                Logger.Warn("Invalid login credentials for {Username}", new { exception.Username });
                return new ErrorResult(400, "Invalid Username or Password");
            }
            catch (Exception exception)
            {
                return new ErrorResult(403, exception.Message);
            }
        }
    }
}